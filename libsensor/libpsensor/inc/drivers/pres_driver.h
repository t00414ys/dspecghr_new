/*!******************************************************************************
 * @file    pres_driver.h
 * @brief   sample program for pressure sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PRES_DRIVER_H__
#define __PRES_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_HSPPAD031)
#include "hsppad031_api.h"
#define	HSPPAD031_DATA	{	hspp031_init,			hspp031_ctrl,			hspp031_rcv ,		hspp031_conv, 	\
							hspp031_setparam,		hspp031_get_ver,		hspp031_get_name ,		0,					\
							{hspp031_get_condition,	0,						0,					0}					},
#else
#define	HSPPAD031_DATA
#endif

#if defined(USE_ZPA2326)
#include "zpa2326_api.h"
#define	ZPA2326_DATA	{	zpa2326_init,			zpa2326_ctrl,			zpa2326_rcv ,		zpa2326_conv, 	\
							zpa2326_setparam,		zpa2326_get_ver,		zpa2326_get_name ,	0,					\
							{zpa2326_get_condition,	0,						0,					0}					},
#else
#define	ZPA2326_DATA
#endif

#if (defined(USE_LPS25H)||defined(USE_LPS25HB))
#include "lps25h_api.h"
#define	LPS25H_DATA		{	lps25h_init,			lps25h_ctrl,			lps25h_rcv ,		lps25h_conv, 	\
							lps25h_setparam,		lps25h_get_ver,			lps25h_get_name ,	0,					\
							{lps25h_get_condition,	0,						0,					0}					},
#else
#define	LPS25H_DATA
#endif

#if defined(USE_MPL115A2)
#include "mpl115a2_api.h"
#define	MPL115A2_DATA	{	mpl115a2_init,			mpl115a2_ctrl,			mpl115a2_rcv ,		mpl115a2_conv, 	\
							mpl115a2_setparam,		mpl115a2_get_ver,		mpl115a2_get_name ,	0,					\
							{mpl115a2_get_condition,0,						0,					0}					},
#else
#define	MPL115A2_DATA
#endif

#if (defined(USE_BMP280) || defined(USE_BME280))
#include "bmp280_api.h"
#define	BMP280_DATA		{	bmp280_init,			bmp280_ctrl,			bmp280_rcv ,		bmp280_conv, 	\
							bmp280_setparam,		bmp280_get_ver,			bmp280_get_name,	0,						\
							{bmp280_get_condition,		0,						0,					0}					},
#else
#define	BMP280_DATA
#endif

#if defined(USE_GTAP200)
#include "gtap200_api.h"
#define	GTAPXXX_DATA	{	gtapxxx_init,			gtapxxx_ctrl,			gtapxxx_rcv ,		gtapxxx_conv, 	\
							gtapxxx_setparam,		gtapxxx_get_ver,			gtapxxx_get_name,	0,						\
							{gtapxxx_get_condition,		0,						0,					0}					},
#else
#define	GTAPXXX_DATA
#endif

#if defined(USE_HP203B)
#include "hp203b_api.h"
#define	HP203B_DATA		{	hp203b_init,			hp203b_ctrl,			hp203b_rcv ,		hp203b_conv, 	\
							hp203b_setparam,		hp203b_get_ver,		hp203b_get_name ,		0,					\
							{hp203b_get_condition,	0,						0,					0}					},
#else
#define	HP203B_DATA
#endif

#if defined(USE_PRES_EMU)
#include "presemu_api.h"
#define	PRESEMU_DATA	{	presemu_init,			0,						presemu_rcv ,		presemu_conv, 		\
							presemu_setparam,		presemu_get_ver,		presemu_get_name ,	0,					\
							{presemu_get_condition,	0,						0,					0}					},
#else
#define	PRESEMU_DATA
#endif


#endif

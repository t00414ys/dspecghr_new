/*!******************************************************************************
 * @file    adc_driver.h
 * @brief   sample program for adc sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADC_DRIVER_H__
#define __ADC_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_HY3118)
#include "hy3118_api.h"
#if	!defined(MAP_HY3118)
#define	MAP_HY3118		(0)
#endif
#define	HY3118_DATA		{	adc_hy3118_init,				adc_hy3118_ctrl,		adc_hy3118_rcv ,		adc_hy3118_conv, 	\
							adc_hy3118_setparam,			adc_hy3118_get_ver,		adc_hy3118_get_name ,	MAP_HY3118,		\
							{adc_hy3118_get_condition,		0,						0,						0}					},
#else
#define	HY3118_DATA
#endif


#endif /* __ADC_DRIVER_H__ */

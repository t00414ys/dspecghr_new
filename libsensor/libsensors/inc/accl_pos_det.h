/*!******************************************************************************
 * @file    accl_pos_det.h
 * @brief   virtual accel pos_det sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup ACCEL_POS_DET ACCEL POS DET
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief accel posture detector sensor<br>
 *  @brief -# <B>Contents</B><br>
 *  @brief A sensor for taking out a fall down counter.<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_ACCEL_POS_DET<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_ACCEL_FALL_DOWN<br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_ACCEL_POS_DET" color=red];
 *		struct2 [label="SENSOR_ID_ACCEL_FALL_DOWN" color=blue];
 *		struct2 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Detection Timing : On-changed (This sensor type output sensor
 * when sensor data have a change.)
 */
#ifndef __ACCL_POS_DET_H__
#define __ACCL_POS_DET_H__

#include "frizz_type.h"
#include "sensor_if.h"
#include "libsensors_id.h"

/** @defgroup ACCEL_POS_DET ACCEL POS DET
 *  @{
 */

/**
 *@brief	Initialize accel posture detector sensor
 *@par		External public functions
 *
 *@retval	sensor_if_t		Sensor Interface
 *
 */
sensor_if_t* accl_pos_det_init( void );

/** @} */
#endif

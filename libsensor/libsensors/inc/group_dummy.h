/*!******************************************************************************
 * @file    group_dummy.h
 * @brief   group of virtual dummy sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief physical sensor header group
 */

#ifndef __GROUP_DUMMY_H__
#define __GROUP_DUMMY_H__

#if !defined(RUN_ON_PC)
#include "queue_in.h"
#else
#include "std_in.h"
#endif

#include "accl_dummy.h"
#include "magn_dummy.h"
#include "gyro_dummy.h"

#include "hub_mgr.h"

#if !defined(RUN_ON_PC)
static void dummy_init( int ( *pop )( void ) )
{
	hub_mgr_regist_sensor( queue_in_init( pop ) );
	hub_mgr_regist_sensor( accl_dummy_init() );
	hub_mgr_regist_sensor( magn_dummy_init() );
	hub_mgr_regist_sensor( gyro_dummy_init() );
}
#endif

#endif

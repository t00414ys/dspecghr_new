/*!******************************************************************************
 * @file    isp_sen_if.h
 * @brief   virtual presence sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ISP_SEN_IF_H__
#define __ISP_SEN_IF_H__

#include "libsensors_id.h"

/** @defgroup ISP ISP
 *  @{
 */
#define ISP_ID	SENSOR_ID_ISP	//!< A ISP sensor interface ID

/**
 * @enum isp_sen_status_e
 * @brief Output data structure for presence sensor status
 */
typedef enum {
	ISP_SEN_REST = 0,			//!< resting
	ISP_SEN_STOP,			//!< stopping
	ISP_SEN_WALK,			//!< walking
	ISP_SEN_RUN,			//!< running
	ISP_SEN_VEHICLE,		//!< vehicle
	ISP_SEN_STAT_MAX		//!< Delimiter
} isp_sen_status_e;

/**
 * @struct isp_sen_data_t
 * @brief Output data structure for presence sensor
 */
typedef struct {
	isp_sen_status_e	status;		//!< action state
} isp_sen_data_t;
/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif

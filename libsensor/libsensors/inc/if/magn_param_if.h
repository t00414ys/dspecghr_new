/*!******************************************************************************
 * @file    magn_param_if.h
 * @brief   virtual magnet parameter sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MAGN_PARAM_IF_H__
#define __MAGN_PARAM_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup MAGNET_PARAMETER MAGNET PARAMETER
 *  @{
 */
//#define USE_MCC_MAG_CALIBRATION		//!< Use MCC version calibration

#define MAGNET_PARAMETER_ID	SENSOR_ID_MAGNET_PARAMETER  //!< magnet calibration parameter

/**
 * @struct magnet_parameter_data_t
 * @brief 	Output data structure for magnet parameter sensor
 */
typedef struct {
	FLOAT		param[12];	//!< 0~8: soft iron parameter(compensation 3x3 matrix), 9~11:hard iron parameter(offset)
	int			f_calib;	//!< 0: uncalibrated, 1:calibrated
} magnet_parameter_data_t;

/**
 * @name Command List
 */
//@{
/**
 * @brief Get Calibration Status
 * @param cmd_code MAGNET_PARAMETER_CMD_GET_STATUS
 * @return 0: uncalibrated, 1: calibrated
 */
#define MAGNET_PARAMETER_CMD_GET_STATUS		0x00

/**
 * @brief Set Parameter for Calibration
 * @param cmd_code MAGNET_PARAMETER_CMD_SET_PARAMETER
 * @return 0:OK
 */
#define MAGNET_PARAMETER_CMD_SET_PARAMETER		0x01
//@}

/** @} */
#endif

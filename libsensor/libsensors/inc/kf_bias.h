/*!******************************************************************************
 * @file    kf_bias.h
 * @brief   Estimation of Bias & Gravity & Rotation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __KF_BIAS_H__
#define __KF_BIAS_H__
#include "kalman_filter.h"
#include "frizz_fp4w.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Handler for kf_bias
 */
typedef void* KF_Bias_h;

/**
 * @brief Create Kalmanfilter for posture estimation
 *
 * @return Handler for kf_bias
 */
KF_Bias_h KF_BiasNew( void );

/**
 * @brief Delete Kalmanfilter for posture estimation
 *
 * @param [in][out] h Handler for kf_bias
 */
void KF_BiasDelete( KF_Bias_h h );

/**
 * @brief Get handler for Kalmanfilter API
 *
 * @param [in][out] h Handler for kf_bias
 *
 * @return handler for Kalmanfilter API
 */
KF_h KF_BiasGetKF( KF_Bias_h h );

/**
 * @brief Initilize kf_bias
 *
 * @param [in][out] h Handler for kf_bias
 */
void KF_BiasInit( KF_Bias_h h );

/**
 * @brief set kalman filter state
 *
 * @param [in][out] h Handler for kf_bias
 * @param [in] var_gyro_ofst Value of Error Variance about gyro zero data
 * @param [in] gyro 3-Axis gyroscope data 0:x, 1:y, 2:z, 3:should be zero
 *
 * @note input sensor data when status is nearly stable.
 */
void KF_BiasSetState( KF_Bias_h h, frizz_fp var_gyro_ofst, frizz_fp4w *gyro );

/**
 * @brief set posture
 *
 * @param [in][out] h Handler for kf_bias
 * @param [in] accl 3-Axis accelerometer data 0:x, 1:y, 2:z, 3:should be zero
 * @param [in] magn 3-Axis magnetometer data 0:x, 1:y, 2:z, 3:should be zero. If NULL it don't use it.
 *
 * @note input sensor data when status is nearly stable.
 */
void KF_BiasSetPosture( KF_Bias_h h, frizz_fp4w *accl, frizz_fp4w *magn );

/**
 * @brief Execution of predection
 *
 * @param [in][out] h Handler for kf_bias
 * @param [in] gyro 3-Axis gyroscope data 0:x, 1:y, 2:z, 3:should be zero
 * @param [in] dtm sampling interval time
 */
void KF_BiasPredict( KF_Bias_h h, frizz_fp4w *gyro, frizz_fp dtm );

/**
 * @brief Execution of measurement
 *
 * @param [in][out] h Handler for kf_bias
 * @param [in] accl 3-Axis accelerometer data 0:x, 1:y, 2:z, 3:should be zero
 * @param [in] magn 3-Axis magnetometer data 0:x, 1:y, 2:z, 3:should be zero. If NULL it don't use it.
 */
void KF_BiasMeasure( KF_Bias_h h, frizz_fp4w *accl, frizz_fp4w *magn );

/**
 * @brief Execution of filtering
 *
 * @param [in][out] h Handler for kf_bias
 */
void KF_BiasFiltering( KF_Bias_h h );

/**
 * @brief Get a quaternion from status of kf_bias
 *
 * @param [in][out] h Handler for kf_bias
 *
 * @return quaternion 0:scaler, 1:x, 2:y, 3:z
 */
const frizz_fp4w_t* KF_BiasGetQuaternion( KF_Bias_h h );

/**
 * @brief Get a DCM from status of kf_bias
 *
 * @param [in][out] h Handler for kf_bias
 *
 * @return DCM: DCM * v_sensor => v_global
 */
const frizz_fp4w_t* KF_BiasGetDCM( KF_Bias_h h );

/**
 * @brief Update qurternion as magnetic north matches referrence frame Y-axis
 *
 * @param [in][out] h Handler for kf_bias
 * @param [in] magn Geomagnet vector
 */
void KF_BiasSetMagNorth( KF_Bias_h h, frizz_fp4w magn );

#ifdef __cplusplus
}
#endif

#endif

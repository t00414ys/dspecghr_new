/*!******************************************************************************
 * @file    magn_param.h
 * @brief   virtual magnet paramater sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup MAGNET_PARAMETER MAGNET PARAMETER
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief magnet paramater sensor
 *  @brief -# <B>Contents</B><br>
 *  @brief This sensor for taking out a calibration parameter of a magnet sensor.<br>
 *  <br>
 *  @brief -# <B>Method of Calculation</B><br>
 *  @brief Correction matrixs of soft iron and hard iron are determined by a magnet raw sensor.<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_MAGNET_PARAMETER<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_MAGNET_RAW<br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_MAGNET_PARAMETER" color=red];
 *		struct2 [label="SENSOR_ID_MAGNET_RAW" color=blue];
 *		struct2 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Detection Timing : One-shot (This sensor type output sensor data only once.)
 */

#ifndef __MAGN_PARAM_H__
#define __MAGN_PARAM_H__

#include "frizz_type.h"
#include "sensor_if.h"
#include "libsensors_id.h"
#include "if/magn_param_if.h"

/** @defgroup MAGNET_PARAMETER MAGNET PARAMETER
 *  @{
 */

/**
 *@brief	Initialize magnet parameter sensor
 *@par		External public functions
 *
 *@retval	sensor_if_t		Sensor Interface
 *
 */
sensor_if_t* magn_param_init( void );

#ifndef USE_MCC_MAG_CALIBRATION

/**
 *@brief	Set calibration status
 *@par		External public functions
 *
 *@param 	f_calib 0:uncalibrated, 1:calibrated
 *
 *@retval	void
 *
 */
void magn_param_set_calibration_status( int f_calib );

/**
 *@brief	Set calibration parameter
 * @brief	If NULL pointer set, it will not be use.
 *@par		External public functions
 *
 * @param soft soft iron parameter(soft[9] : compensation 3x3 matrix)
 * @param hard hard iron parameter(hard[3] : offset)
 *
 *@retval	void
 *
 */
void magn_param_set_calibration_parameter( frizz_fp *soft, frizz_fp *hard );
#endif
/** @} */
#endif

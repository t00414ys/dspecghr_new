/*!******************************************************************************
 * @file    orientation.c
 * @brief   virtual orientation sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_const.h"
#include "frizz_math.h"
#include "quaternion_base.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "orientation.h"
#include "libsensors_id.h"
#include "if/orientation_if.h"
#include "if/magn_param_if.h"
#include "if/magn_calib_raw_if.h"

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[2];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_par;
	sensor_if_get_t		*p_magn_calib_raw;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	// data
	frizz_fp			data[3];
} device_sensor_t;

static device_sensor_t g_device;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 3;
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == g_device.par_ls[0] ) {
		g_device.p_par = gettor;
	}
	if( gettor->id() == g_device.par_ls[1] ) {
		g_device.p_magn_calib_raw = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_par->id(), g_device.tick );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_par->id(), g_device.f_active );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_magn_calib_raw->id(), g_device.f_active );
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.f_need = 1;
	return ts + g_device.tick;
}
/*
 * azimuth: North:0, East:90, South:180, West:270
 * pitch: toward down is positive, toward up is negative, range : -180 <= pitch <= 180
 * roll: toward right down is negative, toward left down is positive, range: -90<= roll <= 90
 */
static int calculate( void )
{
	frizz_fp4w_t *pq, dcm[3];
	magnet_calib_raw_data_t *p_magn_raw;

	g_device.p_par->data( ( void** )&pq, &g_device.ts );
	quaternion_base_create_dcm( pq, dcm );
	g_device.p_magn_calib_raw->data( ( void** )&p_magn_raw, &g_device.ts );

	if( p_magn_raw->status.quality < 2 ) {
		g_device.data[0] = as_frizz_fp( 0 );
		g_device.data[1] = as_frizz_fp( 0 );
		g_device.data[2] = as_frizz_fp( 0 );

	} else {
		// quaternion
		g_device.data[0] = -FRIZZ_MATH_RAD2DEG * frizz_atan2( -dcm[0].z[1], dcm[1].z[1] );
		g_device.data[1] = -FRIZZ_MATH_RAD2DEG * frizz_asin( dcm[2].z[1] );
		g_device.data[2] = -FRIZZ_MATH_RAD2DEG * frizz_atan2( -dcm[2].z[0], dcm[2].z[2] );

		if( g_device.data[0] < as_frizz_fp( 0 ) ) {
			g_device.data[0] += FRIZZ_CONST_360;
		}

		if( g_device.data[1] < -FRIZZ_CONST_90 ) {
			g_device.data[1] += ( -FRIZZ_CONST_TWO * ( FRIZZ_CONST_90 + g_device.data[1] ) );
		} else if( g_device.data[1] > FRIZZ_CONST_90 ) {
			g_device.data[1] += ( FRIZZ_CONST_TWO * ( FRIZZ_CONST_90 - g_device.data[1] ) );
		}

		if( g_device.data[2] < -FRIZZ_CONST_90 ) {
			g_device.data[2] = ( FRIZZ_CONST_180 + g_device.data[2] );
		} else if( g_device.data[2] > FRIZZ_CONST_90 ) {
			g_device.data[2] = ( -FRIZZ_CONST_180 + g_device.data[2] );
		}
	}
	g_device.f_need = 0;
	return 1;
}

sensor_if_t* DEF_INIT( orientation )( void )
{
	// ID
	g_device.id = ORIENTATION_ID;
	g_device.par_ls[0] = SENSOR_ID_ROTATION_VECTOR;
	g_device.par_ls[1] = SENSOR_ID_MAGNET_CALIB_RAW;
	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	g_device.tick = 10;
	g_device.f_need = 0;
	g_device.ts = 0;

	return &( g_device.pif );
}


/*!******************************************************************************
 * @file mes.c
 * @brief source code for message register
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/xtruntime.h>
#include <xtensa/hal.h>
#include <xtensa/tie/xt_interrupt.h>

#include "frizz_peri.h"
#include "gpio.h"
#include "mes.h"

typedef struct {
	mes_callback pf;
	void* arg;
} s_MesInfo;
s_MesInfo g_mes_info;

static void mesISR( void )
{
	*REGMES_H = *REGMES_L = 0;	// clear interrupt
	if( g_mes_info.pf != 0 ) {
		g_mes_info.pf( g_mes_info.arg, mes_get_data() );
	}
}

/**
 * @brief mes initialize
 *
 * @param pf callback function
 * @param arg callback function args
 */
void mes_init( mes_callback pf, void* arg )
{
	mes_end();
	*REGMES_H = *REGMES_L = 0;	// clear interrupt
	g_mes_info.pf = pf;
	g_mes_info.arg = arg;
	_xtos_set_interrupt_handler( INTERRUPT_NO_MESSAGE, ( _xtos_handler )mesISR );
	_xtos_ints_on( 1 << INTERRUPT_NO_MESSAGE );
}

/**
 * @brief mes end
 */
void mes_end( void )
{
	_xtos_ints_off( 1 << INTERRUPT_NO_MESSAGE );
	g_mes_info.pf = 0;
	g_mes_info.arg = 0;
}

/**
 * @brief get message data
 *
 * @return message data
 */
unsigned int mes_get_data( void )
{
	return ( *REGMES_H << 16 ) | *REGMES_L;
}


/*!******************************************************************************
 * @file mes.c
 * @brief source code for message register
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/hal.h>
#include "counter.h"

/**
 * @brief counter initialize
 *
 * @param h handle
 *
 * @note required to call function side secure object for malloc removal.
 */
void frizz_counter_init( FrizzCounter_h* h )
{
	h->cur = 0;
	h->max = 0;
	h->min = -1;
	h->cnt = 0;
	h->ttl = 0;
}

/**
 * @brief start measurement
 *
 * @param h handle
 */
void frizz_counter_start( FrizzCounter_h* h )
{
	h->cur = xthal_get_ccount();
}

/**
 * @brief stop measurement
 *
 * @param h handle
 */
void frizz_counter_stop( FrizzCounter_h* h )
{
	h->cur = xthal_get_ccount() - h->cur;
	if( h->max < h->cur ) {
		h->max = h->cur;
	}
	if( h->cur < h->min ) {
		h->min = h->cur;
	}
	h->cnt++;
	h->ttl += h->cur;
}

/**
 * @brief get calculation value last cycles
 *
 * @param h handle
 *
 * @return last cycles
 */
unsigned int frizz_counter_get_cur( FrizzCounter_h* h )
{
	return h->cur;
}

/**
 * @brief get calculation value  Max cycles
 *
 * @param h handle
 *
 * @return Max cycles
 */
unsigned int frizz_counter_get_max( FrizzCounter_h* h )
{
	return h->max;
}

/**
 * @brief get calculation value Min cycles
 *
 * @param h handle
 *
 * @return Min cycles
 */
unsigned int frizz_counter_get_min( FrizzCounter_h* h )
{
	return h->min;
}

/**
 * @brief calculation count
 *
 * @param h handle
 *
 * @return count value
 */
unsigned int frizz_counter_get_cnt( FrizzCounter_h* h )
{
	return h->cnt;
}

/**
 * @brief get calculation value Total cycle
 *
 * @param h handle
 *
 * @return Total Cycle
 */
unsigned int frizz_counter_get_total( FrizzCounter_h* h )
{
	return h->ttl;
}

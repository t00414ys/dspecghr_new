/*!******************************************************************************
 * @file    mag3110.c
 * @brief   mag3110 physical sensor driver
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "timer.h"
#include "mag3110.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_MAG3110

#define		DRIVER_VER_MAJOR		(1)				// Major Version
#define		DRIVER_VER_MINOR		(0)				// Minor Version
#define		DRIVER_VER_DETAIL		(0)				// Detail Version

// mag3110参考
#if 1
struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[6];	// transmission buffer
	unsigned char		addr;
	unsigned char		device_id;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[3];
	//
} g_mag3110;
#endif

// ads8332参考
#if 0
 struct {
	frizz_fp			scale_accl;		// scaler
	frizz_fp			scale_gyro;		// scaler
	unsigned char		buff[18];	// transmission buffer
	unsigned char		addr;
	unsigned char		device_id;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[6];
	//
} g_ads8332;
#endif


#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char	g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	MAG3110_ADDRESS,
};


int mag3110_init( unsigned int param )
{
	int ret, i;
	unsigned char buff = 0;
	unsigned char buff_who_check = 0;
	unsigned char buff_read = 0;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	g_mag3110.scale = as_frizz_fp( MAG3110_SCALE );

	/* setting direction default set */
	if( param == 0 ) {
		g_mag3110.setting.map_x		= DEFAULT_MAP_X;
		g_mag3110.setting.map_y		= DEFAULT_MAP_Y;
		g_mag3110.setting.map_z		= DEFAULT_MAP_Z;
		g_mag3110.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_mag3110.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_mag3110.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_mag3110.setting.map_x,		g_mag3110.setting.map_y,		g_mag3110.setting.map_z,
					g_mag3110.setting.negate_x,	g_mag3110.setting.negate_y,	g_mag3110.setting.negate_z );
	}

	/* recognition */
	// mc3413
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_mag3110.addr =  add_tbl[i];
		g_mag3110.device_id = 0;			// read id
		ret = i2c_read( g_mag3110.addr, MAG3110_WHO_AM_I, &g_mag3110.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			//buff_who_check = g_mc3413.device_id & MC3413_WHOAMI_CHECKID;
			if( g_mag3110.device_id == MAG3110_WHO_AM_I_ID ) {
				break;
			}
		}
	}

	//buff_who_check = g_mc3413.device_id & MC3413_WHOAMI_CHECKID;
	if( g_mag3110.device_id != MAG3110_WHO_AM_I_ID ) {
		return RESULT_ERR_INIT;
	}
	// @@

	/* Sample Rate */
	ret = i2c_read( g_mag3110.addr, MAG3110_CTRL_REG1, &buff_read, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}
	//buff_read = buff_read & MC3413_SRTFR_RATE_OFF;
	//buff = buff_read | MC3413_SRTFR_RATE;
	buff = MAG3110_DATA_RATE;
	ret = i2c_write( g_mag3110.addr, MAG3110_CTRL_REG1, &buff, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	/* Range Resolution */
	//ret = i2c_write( g_mc3413.addr, MC3413_REG_OUTCFG, &buff, 1 );
	//if( ret != I2C_RESULT_SUCCESS ) {
	//	return RESULT_ERR_INIT;
	//}

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}


void mag3110_ctrl_accl( int f_ena )
{
	unsigned char buff;
	unsigned char buff_read;

	i2c_read( g_mag3110.addr, MAG3110_CTRL_REG1, &buff_read, 1 );

	if( f_ena ) {
		buff_read = buff_read & MAG3110_MODE_STANDBY;
		buff = buff_read | MAG3110_MODE_ACTIVE;		// State Standby00 -> Wake01
	} else {
		buff = buff_read & MAG3110_MODE_STANDBY;	// State Wake01 -> Standby00
	}

	i2c_write( g_mag3110.addr, MAG3110_CTRL_REG1, &buff, 1 );
}


unsigned int mag3110_rcv_accl( unsigned int tick )
{
	//unsigned char buff_read = 0;

	//g_mag3110.recv_result = i2c_read( g_mag3110.addr, MAG3110_REG_SR, &buff_read, 1 );
	//if( g_mag3110.recv_result != 0 ) {
	//	g_mag3110.device_condition |= D_RAW_DEVICE_ERR_READ;
	//	return 0;
	//}

	//buff_read = buff_read & MC3413_ACQ_INT;
	
	g_mag3110.recv_result = i2c_read( g_mag3110.addr, MAG3110_OUT_X_MSB, g_mag3110.buff, sizeof( g_mag3110.buff ) );

	//g_bma2xx.recv_result = i2c_read( g_bma2xx.addr, BMA2x2_X_AXIS_LSB_REG, &g_bma2xx.buff[0], sizeof( g_bma2xx.buff ) );
	//g_mpuxxxx.gyro.recv_result = i2c_read( g_mpuxxxx.addr, MPUXXXX_TEMP_OUT_H, &g_mpuxxxx.gyro.buff[0], 8 );
	//g_mc3413.recv_result = i2c_read( g_mc3413.addr, MC3413_REG_XYZOUT, g_mc3413.buff, sizeof( g_mc3413.buff ) );
	
	if( g_mag3110.recv_result == 0 ) {
		g_mag3110.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_mag3110.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}


int mag3110_conv_accl( frizz_fp data[3] )
{
	frizz_fp4w f4w_buff;			// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;
	sensor_util_half_t s_buff[3];	// x:0, y:1, z:2

	if( g_mag3110.recv_result != 0 ) {
		data[0] = g_mag3110.lasttime_data[0];
		data[1] = g_mag3110.lasttime_data[1];
		data[2] = g_mag3110.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Magnet using mag3110 */
	// to half @ little endian
	s_buff[0].ubyte[1] = g_mag3110.buff[0];	// XOUT_EX_H[15:8]
	s_buff[0].ubyte[0] = g_mag3110.buff[1];	// XOUT_EX_L[0:7]
	s_buff[1].ubyte[1] = g_mag3110.buff[2];	// YOUT_EX_H[15:8]
	s_buff[1].ubyte[0] = g_mag3110.buff[3];	// YOUT_EX_L[0:7]
	s_buff[2].ubyte[1] = g_mag3110.buff[4];	// ZOUT_EX_H[15:8]
	s_buff[2].ubyte[0] = g_mag3110.buff[5];	// ZOUT_EX_L[0:7]

	// to 4way float
	// {Dx, Dy, Dz} = {Sx, Sy, Sz} adjust sensor and board angle.
	fp[0] = ( float )( g_mag3110.setting.negate_x ? ( -s_buff[g_mag3110.setting.map_x].half ) : ( s_buff[g_mag3110.setting.map_x].half ) );
	fp[1] = ( float )( g_mag3110.setting.negate_y ? ( -s_buff[g_mag3110.setting.map_y].half ) : ( s_buff[g_mag3110.setting.map_y].half ) );
	fp[2] = ( float )( g_mag3110.setting.negate_z ? ( -s_buff[g_mag3110.setting.map_z].half ) : ( s_buff[g_mag3110.setting.map_z].half ) );

	// convert
	f4w_buff = g_mag3110.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];

	g_mag3110.lasttime_data[0] = data[0];
	g_mag3110.lasttime_data[1] = data[1];
	g_mag3110.lasttime_data[2] = data[2];

	return RESULT_SUCCESS_CONV;
}

//set axis of device
int mag3110_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_mag3110.setting.map_x		= setting->map_x;
		g_mag3110.setting.map_y		= setting->map_y;
		g_mag3110.setting.map_z		= setting->map_z;
		g_mag3110.setting.negate_x	= setting->negate_x;
		g_mag3110.setting.negate_y	= setting->negate_y;
		g_mag3110.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

// get device condition
int mag3110_get_condition( void *data )
{
	return g_mag3110.device_condition;
}

int mag3110_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_mag3110.buff[0] << 8 ) | g_mag3110.buff[1] );
	temp_buffer[1] = ( short )( ( g_mag3110.buff[2] << 8 ) | g_mag3110.buff[3] );
	temp_buffer[2] = ( short )( ( g_mag3110.buff[4] << 8 ) | g_mag3110.buff[5] );

	return	RESULT_SUCCESS_SET;
}

//get device version
unsigned int mag3110_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

//get device name
unsigned int mag3110_get_name()
{
//	return	D_DRIVER_NAME;
}



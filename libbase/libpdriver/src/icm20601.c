/*!******************************************************************************
 * @file    icm20601.c
 * @brief   icm20601 physical sensor driver
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "timer.h"
#include "icm20601.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_ICM20601

#define		DRIVER_VER_MAJOR		(1)				// Major Version
#define		DRIVER_VER_MINOR		(0)				// Minor Version
#define		DRIVER_VER_DETAIL		(0)				// Detail Version

// mag3110参考
#if 0
struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[6];	// transmission buffer
	unsigned char		addr;
	unsigned char		device_id;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[3];
	//
} g_icm20601;
#endif

// ads8332参考
#if 1
 struct {
	frizz_fp			scale_accl;		// scaler
	frizz_fp			scale_gyro;		// scaler
	unsigned char		buff[18];	// transmission buffer
	unsigned char		addr;
	unsigned char		device_id;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[6];
	//
} g_icm20601;
#endif

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char	g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	ICM20601_ADDRESS,
};


int icm20601_init( unsigned int param )
{
	int ret, i;
	unsigned char buff = 0;
	unsigned char buff_who_check = 0;
	unsigned char buff_read = 0;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	g_icm20601.scale_accl = as_frizz_fp( ICM20601_SCALE_ACCEL );
	g_icm20601.scale_gyro = as_frizz_fp( ICM20601_SCALE_GYRO );

//	g_icm20601.scale_accl = as_frizz_fp( ICM20601_ACCEL_CFG );
//	g_icm20601.scale_gyro = as_frizz_fp( ICM20601_GYRO_CFG );

	/* setting direction default set */
	if( param == 0 ) {
		g_icm20601.setting.map_x		= DEFAULT_MAP_X;
		g_icm20601.setting.map_y		= DEFAULT_MAP_Y;
		g_icm20601.setting.map_z		= DEFAULT_MAP_Z;
		g_icm20601.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_icm20601.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_icm20601.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_icm20601.setting.map_x,		g_icm20601.setting.map_y,		g_icm20601.setting.map_z,
					g_icm20601.setting.negate_x,	g_icm20601.setting.negate_y,	g_icm20601.setting.negate_z );
	}

	/* recognition */
	// mc3413
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_icm20601.addr =  add_tbl[i];
		g_icm20601.device_id = 0;			// read id
		ret = i2c_read( g_icm20601.addr, ICM20601_WHO_AM_I, &g_icm20601.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			//buff_who_check = g_mc3413.device_id & MC3413_WHOAMI_CHECKID;
			if( g_icm20601.device_id == ICM20601_WHO_AM_I_ID ) {
				break;
			}
		}
	}

	//buff_who_check = g_mc3413.device_id & MC3413_WHOAMI_CHECKID;
	if( g_icm20601.device_id != ICM20601_WHO_AM_I_ID ) {
		return RESULT_ERR_INIT;
	}
	// @@

	/* Sample Rate */
	ret = i2c_read( g_icm20601.addr, ICM20601_CONFIG, &buff_read, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}
	//buff_read = buff_read & MC3413_SRTFR_RATE_OFF;
	//buff = buff_read | MC3413_SRTFR_RATE;
	buff = ICM20601_SMPLRT;
	//i2c_write(g_icm20601.addr,ICM20601_GYRO_CONFIG,ICM20601_GYRO_CFG,1);	//gyro 2000dps
	//i2c_write(g_icm20601.addr,ICM20601_ACCEL_CONFIG,ICM20601_ACCEL_CFG,1);	//accel 16g
	ret = i2c_write( g_icm20601.addr, ICM20601_SMPLRT_DIV, &buff, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	/* Range Resolution */
	//ret = i2c_write( g_mc3413.addr, MC3413_REG_OUTCFG, &buff, 1 );
	//if( ret != I2C_RESULT_SUCCESS ) {
	//	return RESULT_ERR_INIT;
	//}

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}


void icm20601_ctrl_accl( int f_ena )
{
	unsigned char buff;
	unsigned char buff_read;

	i2c_read( g_icm20601.addr, ICM20601_PWR_MGMT_2, &buff_read, 1 );

	if( f_ena ) {
		buff_read = buff_read & ICM20601_MODE_STANDBY;
		buff = buff_read | ICM20601_MODE_ACTIVE;		//
	} else {
		buff = buff_read & ICM20601_MODE_STANDBY;	//
	}

	i2c_write( g_icm20601.addr, ICM20601_PWR_MGMT_2, &buff, 1 );
}


unsigned int icm20601_rcv_accl( unsigned int tick )
{
	//unsigned char buff_read = 0;

	//g_icm20601.recv_result = i2c_read( g_icm20601.addr, ICM20601_REG_SR, &buff_read, 1 );
	//if( g_icm20601.recv_result != 0 ) {
	//	g_icm20601.device_condition |= D_RAW_DEVICE_ERR_READ;
	//	return 0;
	//}

	//buff_read = buff_read & MC3413_ACQ_INT;
	
	g_icm20601.recv_result = i2c_read( g_icm20601.addr, ICM20601_ACCEL_XOUT_H, g_icm20601.buff, sizeof( g_icm20601.buff ) );
	//g_bma2xx.recv_result = i2c_read( g_bma2xx.addr, BMA2x2_X_AXIS_LSB_REG, &g_bma2xx.buff[0], sizeof( g_bma2xx.buff ) );
	//g_mpuxxxx.gyro.recv_result = i2c_read( g_mpuxxxx.addr, MPUXXXX_TEMP_OUT_H, &g_mpuxxxx.gyro.buff[0], 8 );
	//g_mc3413.recv_result = i2c_read( g_mc3413.addr, MC3413_REG_XYZOUT, g_mc3413.buff, sizeof( g_mc3413.buff ) );
	
	if( g_icm20601.recv_result == 0 ) {
		g_icm20601.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_icm20601.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}


int icm20601_conv_accl( frizz_fp data[6] )
{
	frizz_fp4w f4w_buff;			// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;


	sensor_util_half_t s_buff[6];	// x:0, y:1, z:2


	if( g_icm20601.recv_result != 0 ) {
		data[0] = g_icm20601.lasttime_data[0];
		data[1] = g_icm20601.lasttime_data[1];
		data[2] = g_icm20601.lasttime_data[2];

		data[3] = g_icm20601.lasttime_data[3];
		data[4] = g_icm20601.lasttime_data[4];
		data[5] = g_icm20601.lasttime_data[5];

		return RESULT_SUCCESS_CONV;
	}

	/* data for Magnet using icm20601 */
	// (どっちがGyroかわからない。)
	// Accel
	s_buff[0].ubyte[1] = g_icm20601.buff[ 0];	// XOUT_EX_H[15:8]
	s_buff[0].ubyte[0] = g_icm20601.buff[ 1];	// XOUT_EX_L[0:7]
	s_buff[1].ubyte[1] = g_icm20601.buff[ 2];	// YOUT_EX_H[15:8]
	s_buff[1].ubyte[0] = g_icm20601.buff[ 3];	// YOUT_EX_L[0:7]
	s_buff[2].ubyte[1] = g_icm20601.buff[ 4];	// ZOUT_EX_H[15:8]
	s_buff[2].ubyte[0] = g_icm20601.buff[ 5];	// ZOUT_EX_L[0:7]

	// Gyro
	s_buff[3].ubyte[1] = g_icm20601.buff[ 6];	// XOUT_EX_H[15:8]
	s_buff[3].ubyte[0] = g_icm20601.buff[ 7];	// XOUT_EX_L[0:7]
	s_buff[4].ubyte[1] = g_icm20601.buff[ 8];	// YOUT_EX_H[15:8]
	s_buff[4].ubyte[0] = g_icm20601.buff[ 9];	// YOUT_EX_L[0:7]
	s_buff[5].ubyte[1] = g_icm20601.buff[10];	// ZOUT_EX_H[15:8]
	s_buff[5].ubyte[0] = g_icm20601.buff[11];	// ZOUT_EX_L[0:7]


#if 1
	// to 4way float
	// {Dx, Dy, Dz} = {Sx, Sy, Sz} adjust sensor and board angle.
	fp[0] = ( float )( g_icm20601.setting.negate_x ? ( -s_buff[g_icm20601.setting.map_x].half ) : ( s_buff[g_icm20601.setting.map_x].half ) );
	fp[1] = ( float )( g_icm20601.setting.negate_y ? ( -s_buff[g_icm20601.setting.map_y].half ) : ( s_buff[g_icm20601.setting.map_y].half ) );
	fp[2] = ( float )( g_icm20601.setting.negate_z ? ( -s_buff[g_icm20601.setting.map_z].half ) : ( s_buff[g_icm20601.setting.map_z].half ) );

	fp[3] = ( float )( g_icm20601.setting.negate_x ? ( -s_buff[g_icm20601.setting.map_x].half ) : ( s_buff[g_icm20601.setting.map_x].half ) );
	fp[4] = ( float )( g_icm20601.setting.negate_y ? ( -s_buff[g_icm20601.setting.map_y].half ) : ( s_buff[g_icm20601.setting.map_y].half ) );
	fp[5] = ( float )( g_icm20601.setting.negate_z ? ( -s_buff[g_icm20601.setting.map_z].half ) : ( s_buff[g_icm20601.setting.map_z].half ) );

#else
	frizz_fp coeff;
	coeff = frizz_div(as_frizz_fp(3.3f), as_frizz_fp(65535.0f));
	// convert accl(16G)
	data[ 0] = frizz_div((coeff * as_frizz_fp(s_buff[0].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));
	data[ 1] = frizz_div((coeff * as_frizz_fp(s_buff[1].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));
	data[ 2] = frizz_div((coeff * as_frizz_fp(s_buff[2].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));
	// convet gyro(1500dps)
	data[ 3] = frizz_div((coeff * as_frizz_fp(s_buff[3].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));
	data[ 4] = frizz_div((coeff * as_frizz_fp(s_buff[4].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));
	data[ 5] = frizz_div((coeff * as_frizz_fp(s_buff[5].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));
#endif

#if 1
	f4w_buff = g_icm20601.scale_accl * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];

	f4w_buff = g_icm20601.scale_gyro * f4w_buff;
	data[3] = fz[3];
	data[4] = fz[4];
	data[5] = fz[5];
#endif


	g_icm20601.lasttime_data[0] = data[0];
	g_icm20601.lasttime_data[1] = data[1];
	g_icm20601.lasttime_data[2] = data[2];
	g_icm20601.lasttime_data[3] = data[3];
	g_icm20601.lasttime_data[4] = data[4];
	g_icm20601.lasttime_data[5] = data[5];


	return RESULT_SUCCESS_CONV;
}

//set axis of device
int icm20601_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_icm20601.setting.map_x		= setting->map_x;
		g_icm20601.setting.map_y		= setting->map_y;
		g_icm20601.setting.map_z		= setting->map_z;
		g_icm20601.setting.negate_x	= setting->negate_x;
		g_icm20601.setting.negate_y	= setting->negate_y;
		g_icm20601.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

// get device condition
int icm20601_get_condition( void *data )
{
	return g_icm20601.device_condition;
}

int icm20601_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_icm20601.buff[0] << 8 ) | g_icm20601.buff[1] );
	temp_buffer[1] = ( short )( ( g_icm20601.buff[2] << 8 ) | g_icm20601.buff[3] );
	temp_buffer[2] = ( short )( ( g_icm20601.buff[4] << 8 ) | g_icm20601.buff[5] );

	return	RESULT_SUCCESS_SET;
}

//get device version
unsigned int icm20601_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

//get device name
unsigned int icm20601_get_name()
{
//	return	D_DRIVER_NAME;
}



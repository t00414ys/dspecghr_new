/*!******************************************************************************
 * @file    ads1191.c
 * @brief   Mc3413 physical sensor driver
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "gpio.h"
#include "frizz_peri.h"
#include "spi.h"
#include <i2c.h>
#include "gpio.h"
#include "timer.h"
#include "ads1191.h"
#include "config_type.h"
#include "icm20601.h"
#include "frizz_math.h"
#include "frizz_const.h"
#include "hub_mgr.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_ADS1191

#define		DRIVER_VER_MAJOR		(1)				// Major Version
#define		DRIVER_VER_MINOR		(0)				// Minor Version
#define		DRIVER_VER_DETAIL		(0)				// Detail Version

struct {
	frizz_fp			scale_accl;		// scaler
	frizz_fp			scale_gyro;		// scaler
	unsigned char		buff[2];	// transmission buffer
	unsigned char		spi_mode;
	unsigned int		cs_no;
	unsigned int		target_freq;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[6];
	//
} g_ads1191;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char	g_init_done = D_INIT_NONE;

//20170913
//int 					gain_dec;
//unsigned char			gain_hex;


//declare a constant for ads1191_reg_write and ads1191_reg_read function.
enum spi_cs_keep {CS_TURN_UP = 0, CS_KEEP = 1};
static const unsigned int cmd_size = 1;
static const unsigned int addr_size = 1;
#if 0
static void ads1191_reg_write( unsigned char addr, unsigned char* tx_data, unsigned char length )
{
	//	unsigned char write_command = 0x0a;
	unsigned char write_command = 0x40;

	unsigned char* no_read = 0;
	spi_trans_data( &write_command, no_read, cmd_size, g_ads1191.cs_no, CS_KEEP );
	spi_trans_data( &addr, no_read, addr_size, g_ads1191.cs_no, CS_KEEP );
	spi_trans_data( tx_data, no_read, length, g_ads1191.cs_no, CS_TURN_UP );
}

static void ads1191_reg_read( unsigned char addr, unsigned char* rx_data, unsigned char length )
{
	//	unsigned char read_command = 0x0b;
	unsigned char read_command = 0x20;

	unsigned char* no_write = 0;
	unsigned char* no_read = 0;
	spi_trans_data( &read_command, no_read, cmd_size, g_ads1191.cs_no, CS_KEEP );
	spi_trans_data( &addr, no_read, addr_size, g_ads1191.cs_no, CS_KEEP );
	spi_trans_data( no_write, rx_data, length, g_ads1191.cs_no, CS_TURN_UP );
}
#endif

int ads1191_init( unsigned int param )
{
	int dummy = 0;
	unsigned char tx_data[4];
	unsigned char read_data[4];
	unsigned int reg_dummy = 0;

	const unsigned char soft_reset_reg_addr = 0x1f;
	unsigned char reset_code = 'R';


	if( param == 0 ) {
		g_ads1191.setting.map_x		= DEFAULT_MAP_X;
		g_ads1191.setting.map_y		= DEFAULT_MAP_Y;
		g_ads1191.setting.map_z		= DEFAULT_MAP_Z;
		g_ads1191.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_ads1191.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_ads1191.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
				g_ads1191.setting.map_x,		g_ads1191.setting.map_y,		g_ads1191.setting.map_z,
				g_ads1191.setting.negate_x,	g_ads1191.setting.negate_y,	g_ads1191.setting.negate_z );
	}
	g_ads1191.cs_no = ADS1191_SPI_CS_NO;	// == GPIO_NO_0
	g_ads1191.target_freq = ADS1191_SPI_FREQ;
	g_ads1191.spi_mode = 2;
	//spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, g_ads1191.spi_mode, g_ads1191.cs_no );
#if 0
	spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, 2, 0 );
	gpio_set_mode( GPIO_NO_0, GPIO_MODE_IN );
	gpio_set_mode( GPIO_NO_1, GPIO_MODE_OUT );	// CONVST (start pin)
	//gpio_set_data( GPIO_NO_1, 1 );				// de-assert
	gpio_set_data( GPIO_NO_1, 0 );
	gpio_set_mode( GPIO_NO_2, GPIO_MODE_IN );	// EOC (DRDY)
	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// reserved
#else
	spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, 2, 0 );
	gpio_set_mode( GPIO_NO_0, GPIO_MODE_IN);
	gpio_set_mode( GPIO_NO_1, GPIO_MODE_OUT );	// CONVST (start pin)
	gpio_set_data( GPIO_NO_1, 0 );				// set start pin low
	gpio_set_mode( GPIO_NO_2, GPIO_MODE_IN );	// EOC (DRDY)
	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// reserved
#endif


	reg_dummy = *REGSPI_CTRL1;

	// 一回目だけ初期値設定を行う
	if( g_init_done == D_INIT_NONE ) {
		// 6倍
		//gain_dec = 6;
		//gain_hex = 0x00;
		//4 * 250 = 1000倍
		gain_dec = 4;
		gain_hex = 0x40;

		temp1 = 0;
		temp2 = 0;

		//return RESULT_SUCCESS_INIT;
	}


	tx_data[0] = ADS1191_STOP;
	spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP);
	tx_data[0] = ADS1191_RESET;
	spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP);
	mdelay(1);
	tx_data[0] = ADS1191_SDATAC;
	tx_data[1] = 0x00;
	spi_trans_data( tx_data, read_data, 2, g_ads1191.cs_no, CS_TURN_UP);	// SDATAC

	tx_data[0] = ADS1191_WREG | ADS1191_CONFIG1;
	tx_data[1] = 0x00;
	tx_data[2] = 0x06;
	//tx_data[2] = 0x86;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// CONFIG1 8kSPS (0x06)	[0000 0110]

	tx_data[0] = ADS1191_WREG | ADS1191_CONFIG2;
	tx_data[1] = 0x00;
	tx_data[2] = 0xA0;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// CONFIG2 Enable internal reference buffer

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF;
	tx_data[1] = 0x00;
	tx_data[2] = 0x10;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// LOFF

	tx_data[0] = ADS1191_WREG | ADS1191_CH1SET;
	tx_data[1] = 0x00;
	//tx_data[2] = 0x00;
	tx_data[2] = gain_hex;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// CH1SET

	tx_data[0] = ADS1191_WREG | ADS1191_CH2SET;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	//tx_data[2] = gain_hex;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// CH2SET

	tx_data[0] = ADS1191_WREG | ADS1191_RLD_SENS;
	tx_data[1] = 0x00;
	tx_data[2] = 0x20;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// RLD_SENS

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF_SENS;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, read_data, 3, 0, CS_TURN_UP);	// LOFF_SENS

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF_STAT;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// LEFF_STAT

	tx_data[0] = ADS1191_WREG | ADS1191_MISC1;
	tx_data[1] = 0x00;
	tx_data[2] = 0x02;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// MISC1

	tx_data[0] = ADS1191_WREG | ADS1191_MISC2;
	tx_data[1] = 0x00;
	tx_data[2] = 0x02;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// MISC2

	tx_data[0] = ADS1191_WREG | ADS1191_GPIO;
	tx_data[1] = 0x00;
	//tx_data[2] = 0x0F;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// GPIO
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// GPIO


	gpio_set_mode( GPIO_NO_0, GPIO_MODE_OUT);
	spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, 2, 0 );


#if 1
	int i;
	for(i=1;i<12;i++){
		tx_data[0] = ADS1191_RREG + i;
		tx_data[1] = 0x00;
		tx_data[2] = 0x00;
		spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);
	}
#endif



	//tx_data[0] = ADS1191_START;
	//spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP);

	gpio_set_data( GPIO_NO_1, 1 );				// set start pin high

	tx_data[0] = ADS1191_RDATAC;
	spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP);



#if 1 //I2C (ICM-20601)
	// i2c initial
	//i2c_init( g_ROSC2_FREQ, 174600 );
	i2c_init( g_ROSC2_FREQ, 400000 );//400kHz
	//unsigned char magreset;
	//unsigned char magbuff;
	//int ret;
	//magreset = MAG3110_AUTO_MRST_EN;
	//magbuff = MAG3110_DATA_RATE;
	//i2c_write(MAG3110_ADDRESS, MAG3110_CTRL_REG2, &magreset, 1); //Enable automatic magnetic sensor reset
	//ret = i2c_write( MAG3110_ADDRESS, MAG3110_CTRL_REG1, &magbuff, 1 );//active mode 80 Hz
#endif

	temp2 = gain_dec;

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}


void ads1191_ctrl_accl( int f_ena )
{

}

void myI2c_callback(void* buff, eI2C_ERR err)
{
	asm("nop");
}

void ads1191_get_accl_data(unsigned char *buf)
{
	unsigned char read_data[5];
	unsigned char read_data2[5];

	unsigned char tx_data[5];
	int i = 0;
	int j = 0;
	char ch = 0;
	int ret;
	unsigned int reg_dummy = 0;
	static unsigned char bSpiInit = 0;

	if ( 0 == bSpiInit ) {
		g_ads1191.cs_no = ADS1191_SPI_CS_NO;	// == GPIO_NO_0
		g_ads1191.target_freq = ADS1191_SPI_FREQ;
		g_ads1191.spi_mode = 2;
		spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, g_ads1191.spi_mode, g_ads1191.cs_no );
		bSpiInit = 1;
	}


	//gpio_set_data( GPIO_NO_1, 0 );				// assert
	//gpio_set_data( GPIO_NO_1, 1 );				// de-assert
	j = 0;


	while( 1 == gpio_get_data(GPIO_NO_2) ){j++;};		// 変換完了待ち

	/* ここでデータを読んでいる?  20170711 fujisawa*/

	gpio_set_data( GPIO_NO_3, 1 );

	tx_data[0] = ADS1191_RDATA;
	//tx_data[0] = 0x00;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	tx_data[3] = 0x00;
	tx_data[4] = 0x00;
	spi_trans_data( tx_data, read_data, 5, g_ads1191.cs_no, CS_TURN_UP );
	//spi_trans_data( tx_data, read_data2, 5, g_ads1191.cs_no, CS_TURN_UP );
	//gpio_set_data( GPIO_NO_3, 1 );

	// MSB (big endian)
	buf[0] = read_data[3];
	buf[1] = read_data[4];

	g_ads1191.recv_result = 0;

}

unsigned int ads1191_rcv_accl( unsigned int tick )
{
	const unsigned char status_reg_addr = 0x0b;
	const unsigned char axis_data_reg_addr = 0x0e;
	const unsigned char data_ready_bit = 0x01;
	unsigned char status;


	ads1191_get_accl_data( g_ads1191.buff );

	return 0;
}
#if 0
static unsigned short deb1[64];
static unsigned char debhead;
unsigned long debsum = 0;
unsigned long avg;
long dif = 0;
#endif
int ads1191_conv_accl( frizz_fp data[12] )
{
	int i;

	//frizz_fp4w f4w_buff1;			// x:0, y:1, z:2
	//frizz_fp* fz1 = ( frizz_fp* )&f4w_buff1;
	//float* fp1 = ( float* )&f4w_buff1;
	//float f_sbuff[3];

	//frizz_fp4w f4w_buff2;			// x:0, y:1, z:2
	//frizz_fp* fz2 = ( frizz_fp* )&f4w_buff2;
	//float* fp2 = ( float* )&f4w_buff2;

	//frizz_fp4w f4w_buff3;			// x:0, y:1, z:2
	//frizz_fp* fz3 = ( frizz_fp* )&f4w_buff3;
	//float* fp3 = ( float* )&f4w_buff3;

	sensor_util_half_t s_buff[2];	// x:0, y:1, z:2
	sensor_util_half_t old_s_buff[2];	// x:0, y:1, z:2


	if( g_ads1191.recv_result != 0 ) {
		data[0] = g_ads1191.lasttime_data[0];
		data[1] = g_ads1191.lasttime_data[1];
		data[2] = g_ads1191.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Accel using ads1191 */
	s_buff[0].ubyte[0] = g_ads1191.buff[ 1];
	s_buff[0].ubyte[1] = g_ads1191.buff[ 0];

	data[ 1] = as_frizz_fp(s_buff[0].uh);
	data[ 2] = as_frizz_fp(s_buff[0].half);
	data[ 5] = frizz_div(as_frizz_fp(2.4f) * as_frizz_fp(s_buff[0].uh), as_frizz_fp(65535.0f));
	//こっちのデータを使う
	data[ 6] = frizz_div(as_frizz_fp(2.4f) * as_frizz_fp(s_buff[0].half), as_frizz_fp(65535.0f));

	g_ads1191.lasttime_data[0] = data[0];
	g_ads1191.lasttime_data[1] = data[1];
	g_ads1191.lasttime_data[2] = data[2];
	g_ads1191.lasttime_data[3] = data[3];
	g_ads1191.lasttime_data[4] = data[4];
	g_ads1191.lasttime_data[5] = data[5];

	return RESULT_SUCCESS_CONV;
}

int ads1191_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
			( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_ads1191.setting.map_x		= setting->map_x;
		g_ads1191.setting.map_y		= setting->map_y;
		g_ads1191.setting.map_z		= setting->map_z;
		g_ads1191.setting.negate_x	= setting->negate_x;
		g_ads1191.setting.negate_y	= setting->negate_y;
		g_ads1191.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int ads1191_get_condition( void *data )
{
	return g_ads1191.device_condition;
}

int ads1191_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( ((unsigned short)(g_ads1191.buff[0])) << 8 ) | g_ads1191.buff[1] );
	//temp_buffer[1] = ( short )( ( ((unsigned short)(g_ads1191.buff[3])) << 8 ) | g_ads1191.buff[2] );
	//temp_buffer[2] = ( short )( ( ((unsigned short)(g_ads1191.buff[5])) << 8 ) | g_ads1191.buff[4] );



	return	RESULT_SUCCESS_SET;
}

unsigned int ads1191_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int ads1191_get_name()
{
	return	D_DRIVER_NAME;
}



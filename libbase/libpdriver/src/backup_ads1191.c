/*!******************************************************************************
 * @file    ads1191.c
 * @brief   Mc3413 physical sensor driver
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "gpio.h"
#include "frizz_peri.h"
#include "spi.h"
#include <i2c.h>
#include "gpio.h"
#include "timer.h"
#include "ads1191.h"
#include "config_type.h"
#include "icm20601.h"
#include "frizz_math.h"
#include "frizz_const.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_ADS1191

#define		DRIVER_VER_MAJOR		(1)				// Major Version
#define		DRIVER_VER_MINOR		(0)				// Minor Version
#define		DRIVER_VER_DETAIL		(0)				// Detail Version

struct {
	frizz_fp			scale_accl;		// scaler
	frizz_fp			scale_gyro;		// scaler
	unsigned char		buff[18];	// transmission buffer
	unsigned char		spi_mode;
	unsigned int		cs_no;
	unsigned int		target_freq;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[6];
	//
} g_ads1191;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char	g_init_done = D_INIT_NONE;


//declare a constant for ads1191_reg_write and ads1191_reg_read function.
enum spi_cs_keep {CS_TURN_UP = 0, CS_KEEP = 1};
static const unsigned int cmd_size = 1;
static const unsigned int addr_size = 1;
#if 1
static void ads1191_reg_write( unsigned char addr, unsigned char* tx_data, unsigned char length )
{
//	unsigned char write_command = 0x0a;
	unsigned char write_command = 0x40;

	unsigned char* no_read = 0;
	spi_trans_data( &write_command, no_read, cmd_size, g_ads1191.cs_no, CS_KEEP );
	spi_trans_data( &addr, no_read, addr_size, g_ads1191.cs_no, CS_KEEP );
	spi_trans_data( tx_data, no_read, length, g_ads1191.cs_no, CS_TURN_UP );
}

static void ads1191_reg_read( unsigned char addr, unsigned char* rx_data, unsigned char length )
{
//	unsigned char read_command = 0x0b;
	unsigned char read_command = 0x20;

	unsigned char* no_write = 0;
	unsigned char* no_read = 0;
	spi_trans_data( &read_command, no_read, cmd_size, g_ads1191.cs_no, CS_KEEP );
	spi_trans_data( &addr, no_read, addr_size, g_ads1191.cs_no, CS_KEEP );
	spi_trans_data( no_write, rx_data, length, g_ads1191.cs_no, CS_TURN_UP );
}
#endif

int ads1191_init__( unsigned int param )
{
	int dummy = 0;
	unsigned char tx_data[4];
	unsigned char read_data[4];
	unsigned int reg_dummy = 0;

	const unsigned char soft_reset_reg_addr = 0x1f;
	unsigned char reset_code = 'R';
	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	if( param == 0 ) {
			g_ads1191.setting.map_x		= DEFAULT_MAP_X;
			g_ads1191.setting.map_y		= DEFAULT_MAP_Y;
			g_ads1191.setting.map_z		= DEFAULT_MAP_Z;
			g_ads1191.setting.negate_x	= SETTING_DIRECTION_ASSERT;
			g_ads1191.setting.negate_y	= SETTING_DIRECTION_ASSERT;
			g_ads1191.setting.negate_z	= SETTING_DIRECTION_ASSERT;
		} else {
			EXPAND_MAP(	param,
					g_ads1191.setting.map_x,		g_ads1191.setting.map_y,		g_ads1191.setting.map_z,
					g_ads1191.setting.negate_x,	g_ads1191.setting.negate_y,	g_ads1191.setting.negate_z );
		}
	g_ads1191.cs_no = ADS1191_SPI_CS_NO;	// == GPIO_NO_0
	g_ads1191.target_freq = ADS1191_SPI_FREQ;
	g_ads1191.spi_mode = 2;
	spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, g_ads1191.spi_mode, g_ads1191.cs_no );

	gpio_set_mode( GPIO_NO_1, GPIO_MODE_OUT );	// CONVST
	gpio_set_data( GPIO_NO_1, 1 );				// de-assert
	gpio_set_mode( GPIO_NO_2, GPIO_MODE_IN );	// EOC
	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// reserved

	reg_dummy = *REGSPI_CTRL1;

//	ads1191_reg_write(ADS1191_STOP,tx_data,0);

//	ads1191_reg_write(ADS1191_WAKEUP,tx_data,0);


#if 0 //レジスタ初期設定 案1
	spi_trans_data("\x11",read_data,1,g_ads1191.cs_no, CS_TURN_UP);	//SDATAC

	spi_trans_data("\x40\x01\x06",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//CONFIG1

	spi_trans_data("\x40\x02\xA0",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//CONFIG2

	spi_trans_data("\x40\x03\x10",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//LOFF

	spi_trans_data("\x40\x04\x00",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//CH1SET

	spi_trans_data("\x40\x05\x00",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//CH2SET

	spi_trans_data("\x40\x06\x20",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//RLD_SENS

	spi_trans_data("\x40\x07\x00",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//LOFF_SENS

	spi_trans_data("\x40\x08\x00",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//LOFF_STAT

	spi_trans_data("\x40\x09\x02",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//MISC1

	spi_trans_data("\x40\x0A\x02",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//MISC2

	spi_trans_data("\x40\x0B\x0F",read_data,3,g_ads1191.cs_no, CS_TURN_UP);	//GPIO


	spi_trans_data("\x08",read_data,1,g_ads1191.cs_no, CS_TURN_UP);	//START

	spi_trans_data("\x10",read_data,1,g_ads1191.cs_no, CS_TURN_UP);	//RDATAC


#elif 0
//	レジスタ初期設定 案2
	ads1191_reg_write(ADS1191_RESET,tx_data,1);


	ads1191_reg_write(ADS1191_SDATAC,tx_data,1);

	tx_data[0] = 0x00;
	tx_data[1] = 0x50;
	ads1191_reg_write(ADS1191_WREG | ADS1191_ID,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x06;
	ads1191_reg_write(ADS1191_WREG | ADS1191_CONFIG1,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0xA0;
	ads1191_reg_write(ADS1191_WREG | ADS1191_CONFIG2,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x10;
	ads1191_reg_write(ADS1191_WREG | ADS1191_LOFF,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x00;
	ads1191_reg_write(ADS1191_WREG | ADS1191_CH1SET,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x00;
	ads1191_reg_write(ADS1191_WREG | ADS1191_CH2SET,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x20;
	ads1191_reg_write(ADS1191_WREG | ADS1191_RLD_SENS,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x00;
	ads1191_reg_write(ADS1191_WREG | ADS1191_LOFF_SENS,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x00;
	ads1191_reg_write(ADS1191_WREG | ADS1191_LOFF_STAT,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x02;
	ads1191_reg_write(ADS1191_WREG | ADS1191_MISC1,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x02;
	ads1191_reg_write(ADS1191_WREG | ADS1191_MISC2,tx_data,3);

	tx_data[0] = 0x00;
	tx_data[1] = 0x0F;
	ads1191_reg_write(ADS1191_WREG | ADS1191_GPIO,tx_data,3);

	ads1191_reg_write(ADS1191_START,tx_data,1);

	ads1191_reg_write(ADS1191_RDATAC,tx_data,1);

#else //レジスタ初期設定 案3

	tx_data[0] = ADS1191_SDATAC;
	tx_data[1] = 0x00;
	spi_trans_data( tx_data, read_data, 2, g_ads1191.cs_no, CS_TURN_UP);	// SDATAC
	spi_trans_data( tx_data, read_data, 2, g_ads1191.cs_no, CS_TURN_UP);	// SDATAC

	tx_data[0] = ADS1191_STOP;
	spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP);	// SDATAC


	tx_data[0] = ADS1191_WREG | ADS1191_CONFIG1;
	tx_data[1] = 0x00;
	tx_data[2] = 0x06;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// CONFIG1 8kSPS (0x06)	[0000 0110]

	tx_data[0] = ADS1191_WREG | ADS1191_CONFIG2;
	tx_data[1] = 0x00;
	tx_data[2] = 0xA0;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// CONFIG2 Enable internal reference buffer

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF;
	tx_data[1] = 0x00;
	tx_data[2] = 0x10;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// LOFF

#if 1	//ゲイン設定するから別関数で書き込み？　0711 fujisawa
	tx_data[0] = ADS1191_WREG | ADS1191_CH1SET;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// CH1SET
#endif

	tx_data[0] = ADS1191_WREG | ADS1191_RLD_SENS;
	tx_data[1] = 0x00;
	tx_data[2] = 0x20;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// RLD_SENS

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF_SENS;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, read_data, 3, 0, CS_TURN_UP);	// LOFF_SENS

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF_STAT;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// LEFF_STAT

	tx_data[0] = ADS1191_WREG | ADS1191_MISC1;
	tx_data[1] = 0x00;
	tx_data[2] = 0x02;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// MISC1

	tx_data[0] = ADS1191_WREG | ADS1191_MISC2;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// MISC2

	tx_data[0] = ADS1191_WREG | ADS1191_GPIO;
	tx_data[1] = 0x00;
	tx_data[2] = 0x0F;
	spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);	// GPIO

	tx_data[0] = ADS1191_START;
	spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP);

	tx_data[0] = ADS1191_RDATAC;
	spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP);
#endif

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}




#if 0
int ads1191_init( unsigned int param )
{
	int dummy = 0;
	unsigned char tx_data[4];
	unsigned char read_data[4];
	unsigned int reg_dummy = 0;

	const unsigned char soft_reset_reg_addr = 0x1f;
	unsigned char reset_code = 'R';
	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	//g_ads1191.scale_accl = as_frizz_fp( ADS1191_ACCL_PER_LSB );

	/* setting direction default set */
	if( param == 0 ) {
		g_ads1191.setting.map_x		= DEFAULT_MAP_X;
		g_ads1191.setting.map_y		= DEFAULT_MAP_Y;
		g_ads1191.setting.map_z		= DEFAULT_MAP_Z;
		g_ads1191.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_ads1191.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_ads1191.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
				g_ads1191.setting.map_x,		g_ads1191.setting.map_y,		g_ads1191.setting.map_z,
				g_ads1191.setting.negate_x,	g_ads1191.setting.negate_y,	g_ads1191.setting.negate_z );
	}

	//gpio_set_mode( GPIO_NO_0, GPIO_MODE_OUT );	// CS
	g_ads1191.cs_no = ADS1191_SPI_CS_NO;	// == GPIO_NO_0
	g_ads1191.target_freq = ADS1191_SPI_FREQ;
	g_ads1191.spi_mode = 2;
	spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, g_ads1191.spi_mode, g_ads1191.cs_no );

#if 1 //I2Cを使わないテスト icm20601ver
	i2c_init( g_ROSC2_FREQ, 174600 );
//	unsigned char magreset;
	unsigned char magbuff;
	unsigned char man1;
	unsigned char man2;
	int ret;
	unsigned char accel;
	unsigned char gyro;
	unsigned char fifo;

	accel = ICM20601_ACCEL_CFG;
	gyro  = ICM20601_GYRO_CFG;
	fifo = 0x78;

	man1 = 0x28;		//PWR_MGMT1 [0010 1000]
	man2 = 0x00;		//PWR_MGMT2 [0000 0000]
	//magreset = ICM20601_AUTO_MRST_EN;
	magbuff = ICM20601_SMPLRT;
	i2c_write(ICM20601_ADDRESS,ICM20601_PWR_MGMT_1,&man1,1);
	i2c_write(ICM20601_ADDRESS,ICM20601_PWR_MGMT_2,&man2,1);

	i2c_write(ICM20601_ADDRESS,ICM20601_ACCEL_CONFIG,&accel,1);
	i2c_write(ICM20601_ADDRESS,ICM20601_GYRO_CONFIG,&gyro,1);

	i2c_write(ICM20601_ADDRESS,ICM20601_FIFO_EN,&fifo,1);
	//i2c_write(ICM20601_ADDRESS, ICM20601_CTRL_REG2, &magreset, 1);
	ret = i2c_write( ICM20601_ADDRESS, ICM20601_SMPLRT_DIV, &magbuff, 1 );
#endif


	//gpio_set_mode( GPIO_NO_0, GPIO_MODE_OUT );	// CS
	//gpio_set_data( GPIO_NO_0, 1 );				// de-assert

	// CONVSTとEOC
	gpio_set_mode( GPIO_NO_1, GPIO_MODE_OUT );	// CONVST
	gpio_set_data( GPIO_NO_1, 1 );				// de-assert
	gpio_set_mode( GPIO_NO_2, GPIO_MODE_IN );	// EOC
	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// reserved

	reg_dummy = *REGSPI_CTRL1;
#if 0
	//gpio_set_data( GPIO_NO_0, 0 );				// assert
	spi_trans_data( "\xB0", read_data, 1, g_ads1191.cs_no, CS_TURN_UP );
	//gpio_set_data( GPIO_NO_0, 1 );				// de-assert

	//( GPIO_NO_0, 0 );				// assert
	spi_trans_data( "\xF0", read_data, 1, g_ads1191.cs_no, CS_TURN_UP );
	//gpio_set_data( GPIO_NO_0, 1 );				// de-assert

	spi_trans_data( "\xC0", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );

	spi_trans_data( "\xE0\x02", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
	spi_trans_data( "\xC0\x00", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
	spi_trans_data( "\xE0\x04", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
	spi_trans_data( "\xC0\x00", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
	spi_trans_data( "\xE0\x08", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
	spi_trans_data( "\xC0\x00", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
	spi_trans_data( "\xE0\x10", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
	spi_trans_data( "\xC0\x00", read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
#else
	ads1191_reg_write(ADS1191_RESET,tx_data,0);
/*
	tx_data[0] = 0x00;
	tx_data[1] = 0x06;
	ads1191_reg_write(ADS1191_WREG | ADS1191_CONFIG1,tx_data,2);

	tx_data[0] = 0x00;
	tx_data[1] = 0xA0;
	ads1191_reg_write(ADS1191_WREG | ADS1191_CONFIG2,tx_data,2);


/*
	tx_data[0] = ADS1191_SDATAC;
	spi_trans_data( tx_data, read_data, 1, 0, CS_TURN_UP);	// SDATAC

	tx_data[0] = ADS1191_WREG | ADS1191_CONFIG1;
	tx_data[1] = 0x00;
	tx_data[2] = 0x06;
	spi_trans_data( tx_data, read_data, 3, 0, CS_TURN_UP);	// CONFIG1 8kSPS (0x06)	[0000 0110]

	//magbuff = *ADS1191_CONFIG1;

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF;
	tx_data[1] = 0x00;
	tx_data[2] = 0x10;
	spi_trans_data( tx_data, read_data, 3, 0, CS_TURN_UP);	// LOFF



	tx_data[0] = ADS1191_WREG | ADS1191_CONFIG2;
	tx_data[1] = 0x00;
	tx_data[2] = 0xA0;
	spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	// CONFIG2 Enable internal reference buffer

	tx_data[0] = ADS1191_WREG | ADS1191_CH1SET;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	// CH1SET

	tx_data[0] = ADS1191_WREG | ADS1191_RLD_SENS;
	tx_data[1] = 0x00;
	tx_data[2] = 0x20;
	spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	// RLD_SENS

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF_SENS;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	// LOFF_SENS

	tx_data[0] = ADS1191_WREG | ADS1191_LOFF_STAT;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	// LEFF_STAT

	tx_data[0] = ADS1191_WREG | ADS1191_MISC1;
	tx_data[1] = 0x00;
	tx_data[2] = 0x02;
	spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	// MISC1

	tx_data[0] = ADS1191_WREG | ADS1191_MISC2;
	tx_data[1] = 0x00;
	tx_data[2] = 0x00;
	spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	// MISC2

	tx_data[0] = ADS1191_WREG | ADS1191_GPIO;
	tx_data[1] = 0x00;
	tx_data[2] = 0x0F;
	spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	// GPIO
*/

	//gpio 初期化
	//gpio_init( 0, 0 );
	//gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// Interrupt Setting
	//_xtos_set_intlevel( INTERRUPT_LEVEL_GPIO3_EDGE );
/*
	// レジスタ読み出し
	for(i = 0; i < 20; i++){
		tx_data[0] = ADS1191_SDATAC;
		spi_trans_data( tx_data, rx_data, 1, 0, CS_TURN_UP);	// SDATAC

		tx_data[0] = ADS1191_RREG + i;
		tx_data[1] = 0x00;
		tx_data[2] = 0x00;
		spi_trans_data( tx_data, rx_data, 3, 0, CS_TURN_UP);	//
	}
	*/
#endif
	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}
#endif

void ads1191_ctrl_accl( int f_ena )
{

}

void myI2c_callback(void* buff, eI2C_ERR err)
{
	asm("nop");
}

void ads1191_get_accl_data(unsigned char *buf)
{
	unsigned char read_data[4];
	unsigned char tx_data[4];
	int i = 0;
	int j = 0;
	char ch = 0;
	int ret;
	unsigned int reg_dummy = 0;
	static unsigned char bSpiInit = 0;

	if ( 0 == bSpiInit ) {
		g_ads1191.cs_no = ADS1191_SPI_CS_NO;	// == GPIO_NO_0
		g_ads1191.target_freq = ADS1191_SPI_FREQ;
		g_ads1191.spi_mode = 2;
		spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, g_ads1191.spi_mode, g_ads1191.cs_no );
		bSpiInit = 1;
	}


		gpio_set_data( GPIO_NO_1, 0 );				// assert
		gpio_set_data( GPIO_NO_1, 1 );				// de-assert
		j = 0;
		while( 0 == gpio_get_data(GPIO_NO_2) ){j++;};		// 変換完了待ち
		//		gpio_set_data( GPIO_NO_0, 0 );

		//ads1191_reg_write(ADS1191_SDATAC,tx_data,1);
//		tx_data[0] = ADS1191_SDATAC;
//		tx_data[1] = 0x00;
//		spi_trans_data( tx_data, read_data, 2, g_ads1191.cs_no, CS_TURN_UP );
//		spi_trans_data( tx_data, read_data, 2, g_ads1191.cs_no, CS_TURN_UP );

//		spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_KEEP );
		//ads1191_reg_read(ADS1191_RREG | ADS1191_CONFIG1,read_data,3);

//		spi_trans_data( "\x12\x00\x00", read_data, 3, g_ads1191.cs_no, CS_TURN_UP );

//		spi_trans_data( "\x20\x02", read_data, 3, g_ads1191.cs_no, CS_TURN_UP );


//		tx_data[0] = ADS1191_RREG | ADS1191_CH1SET;

//		tx_data[0] = ADS1191_WREG | ADS1191_CONFIG2;
//		tx_data[0] = ADS1191_RREG | ADS1191_CONFIG2;
//		tx_data[0] = ADS1191_RDATA;
//		spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP );


/* ここでデータを読んでいる  20170711 fujisawa*/

		tx_data[0] = ADS1191_RDATA;
//		tx_data[0] = 0x00;
		tx_data[1] = 0x00;
		tx_data[2] = 0x00;
//		tx_data[3] = 0x00;
		spi_trans_data( tx_data, read_data, 4, g_ads1191.cs_no, CS_TURN_UP );

		buf[0] = read_data[0];
		buf[1] = read_data[1];


//		spi_trans_data( tx_data, read_data, 4, g_ads1191.cs_no, CS_TURN_UP );

//		buf[1] = buf[1];



//		ads1191_reg_write(*tx_data,read_data,3);

//		spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP );



/*
		ch = ((read_data[2]>>5)&0x07);

		if ( ch < 3 )
		{	// IN6,IN7には何も繋がっていないので取らない
			buf[(ch*2)+0] = read_data[1];
			buf[(ch*2)+1] = read_data[0];
		}
*/

}

#if 0
void ads1191_get_accl_data( unsigned char *buf )
{
#if 1 //speed test
	//gpio_set_data( GPIO_NO_3, 1 );
	unsigned char read_data[4];
	unsigned char tx_data[4];
	int i = 0;
	int j = 0;
	char ch = 0;
	int ret;
	unsigned int reg_dummy = 0;
	static unsigned char bSpiInit = 0;

	if ( 0 == bSpiInit ) {
		g_ads1191.cs_no = ADS1191_SPI_CS_NO;	// == GPIO_NO_0
		g_ads1191.target_freq = ADS1191_SPI_FREQ;
		g_ads1191.spi_mode = 2;
		spi_init( g_ROSC2_FREQ, g_ads1191.target_freq, g_ads1191.spi_mode, g_ads1191.cs_no );
		bSpiInit = 1;
	}

	for ( i = 0; i < 8; i++ ) {
		gpio_set_data( GPIO_NO_1, 0 );				// assert
		gpio_set_data( GPIO_NO_1, 1 );				// de-assert
		j = 0;
		while( 0 == gpio_get_data(GPIO_NO_2) ){j++;};		// 変換完了待ち
		//		gpio_set_data( GPIO_NO_0, 0 );				// assert
#if 0
		spi_trans_data( "\xD0\x00\x00\x00", read_data, 3, g_ads1191.cs_no, CS_TURN_UP );
		//		gpio_set_data( GPIO_NO_0, 1 );				// de-assert
		ch = ((read_data[2]>>5)&0x07);
#else

		ads1191_reg_write(ADS1191_SDATAC,tx_data,0);

		ads1191_reg_read(ADS1191_RREG | ADS1191_CONFIG1,g_ads1191.buff,sizeof(g_ads1191.buff));

/*
		tx_data[0] = ADS1191_SDATAC;
		spi_trans_data( tx_data, read_data, 1, 0, CS_TURN_UP);	// SDATAC

		tx_data[0] = ADS1191_RREG | ADS1191_CONFIG1;
		tx_data[1] = 0x00;
		tx_data[2] = 0x00;
		spi_trans_data( tx_data, read_data, 3, 0, CS_TURN_UP);	//
*/



		/*// レジスタ読み出し
			tx_data[0] = ADS1191_SDATAC;
			spi_trans_data( tx_data, read_data, 1, g_ads1191.cs_no, CS_TURN_UP);	// SDATAC

			tx_data[0] = ADS1191_RREG + i;
			tx_data[1] = 0x00;
			tx_data[2] = 0x00;
			spi_trans_data( tx_data, read_data, 3, g_ads1191.cs_no, CS_TURN_UP);
		*/
		ch = ((read_data[2]>>5)&0x07);
#endif
		if ( ch < 3 ) {	// IN6,IN7には何も繋がっていないので取らない
			buf[(ch*2)+0] = read_data[1];
			buf[(ch*2)+1] = read_data[0];
		}

		asm("nop");
	}
	// get icm20601 data
	int temp_mag;
	//unsigned char			ads1191buff[6];
	//temp_mag = i2c_read( ICM20601_ADDRESS, ICM20601_OUT_X_LSB, &(buf[12]), sizeof( ads1191buff ) );
	//temp_mag = i2c_read( ICM20601_ADDRESS, ICM20601_OUT_X_MSB, &(buf[12]), 6 );


#if 1//icm20601ver
	static sI2C_CMD cmdacc;
	cmdacc.fread = 1;
	cmdacc.saddr = ICM20601_ADDRESS;
	cmdacc.raddr = ICM20601_ACCEL_XOUT_H;
	cmdacc.data = &(buf[6]);
	cmdacc.size = 12;
	i2c_transfer_add( &cmdacc, myI2c_callback, 0 );
#endif

#if 0
	static sI2C_CMD cmdgyro;
	cmdgyro.fread = 1;
	cmdgyro.saddr = ICM20601_ADDRESS;
	cmdgyro.raddr = ICM20601_GYRO_XOUT_H;
	cmdgyro.data = &(buf[6]);
	cmdgyro.size = 12;
	i2c_transfer_add( &cmdgyro, myI2c_callback, 0 );
#endif
#endif //end speed test
	g_ads1191.recv_result = 0;
	//gpio_set_data( GPIO_NO_3, 0 );
}

#endif

unsigned int ads1191_rcv_accl( unsigned int tick )
{
	const unsigned char status_reg_addr = 0x0b;
	const unsigned char axis_data_reg_addr = 0x0e;
	const unsigned char data_ready_bit = 0x01;
	unsigned char status;


	ads1191_get_accl_data( g_ads1191.buff );

	return 0;
}
#if 0
static unsigned short deb1[64];
static unsigned char debhead;
unsigned long debsum = 0;
unsigned long avg;
long dif = 0;
#endif
int ads1191_conv_accl( frizz_fp data[12] )
{
	int i;

	//frizz_fp4w f4w_buff1;			// x:0, y:1, z:2
	//frizz_fp* fz1 = ( frizz_fp* )&f4w_buff1;
	//float* fp1 = ( float* )&f4w_buff1;
	//float f_sbuff[3];

	//frizz_fp4w f4w_buff2;			// x:0, y:1, z:2
	//frizz_fp* fz2 = ( frizz_fp* )&f4w_buff2;
	//float* fp2 = ( float* )&f4w_buff2;

	//frizz_fp4w f4w_buff3;			// x:0, y:1, z:2
	//frizz_fp* fz3 = ( frizz_fp* )&f4w_buff3;
	//float* fp3 = ( float* )&f4w_buff3;



	sensor_util_half_t s_buff[9];	// x:0, y:1, z:2
	sensor_util_half_t old_s_buff[3];	// x:0, y:1, z:2


	if( g_ads1191.recv_result != 0 ) {
		data[0] = g_ads1191.lasttime_data[0];
		data[1] = g_ads1191.lasttime_data[1];
		data[2] = g_ads1191.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Accel using ads1191 */
	s_buff[0].ubyte[0] = g_ads1191.buff[ 0];	// XOUT_EX_L
	s_buff[0].ubyte[1] = g_ads1191.buff[ 1];	// XOUT_EX_H
	s_buff[1].ubyte[0] = g_ads1191.buff[ 2];	// YOUT_EX_L
	s_buff[1].ubyte[1] = g_ads1191.buff[ 3];	// YOUT_EX_H
	s_buff[2].ubyte[0] = g_ads1191.buff[ 4];	// ZOUT_EX_L
	s_buff[2].ubyte[1] = g_ads1191.buff[ 5];	// ZOUT_EX_H

	/* data for Gyro using ads1191 */
	s_buff[3].ubyte[0] = g_ads1191.buff[ 6];	// XOUT_EX_L
	s_buff[3].ubyte[1] = g_ads1191.buff[ 7];	// XOUT_EX_H
	s_buff[4].ubyte[0] = g_ads1191.buff[ 8];	// YOUT_EX_L
	s_buff[4].ubyte[1] = g_ads1191.buff[ 9];	// YOUT_EX_H
	s_buff[5].ubyte[0] = g_ads1191.buff[10];	// ZOUT_EX_L
	s_buff[5].ubyte[1] = g_ads1191.buff[11];	// ZOUT_EX_H

	// mag
#if 0
	s_buff[6].ubyte[0] = g_ads1191.buff[12];		// MAG_X_L
	s_buff[6].ubyte[1] = g_ads1191.buff[13];		// MAG_X_H
	s_buff[7].ubyte[0] = g_ads1191.buff[14];		// MAG_Y_L
	s_buff[7].ubyte[1] = g_ads1191.buff[15];		// MAG_Y_H
	s_buff[8].ubyte[0] = g_ads1191.buff[16];		// MAG_Z_L
	s_buff[8].ubyte[1] = g_ads1191.buff[17];		// MAG_Z_H
#else
	s_buff[6].ubyte[1] = g_ads1191.buff[12];		// MAG_X_H
	s_buff[6].ubyte[0] = g_ads1191.buff[13];		// MAG_X_L
	s_buff[7].ubyte[1] = g_ads1191.buff[14];		// MAG_Y_H
	s_buff[7].ubyte[0] = g_ads1191.buff[15];		// MAG_Y_L
	s_buff[8].ubyte[1] = g_ads1191.buff[16];		// MAG_Z_H
	s_buff[8].ubyte[0] = g_ads1191.buff[17];		// MAG_Z_L
#endif

#if 0
	frizz_fp coeff;
	coeff = frizz_div(as_frizz_fp(3.3f), as_frizz_fp(65535.0f));
	//data[ 1] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[0].uh)/as_frizz_fp(65535.0f))-as_frizz_fp(1.65f))/as_frizz_fp(0.0627f);
	//data[ 2] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[1].uh)/as_frizz_fp(65535.0f))-as_frizz_fp(1.65f))/as_frizz_fp(0.0627f);
	//data[ 3] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[2].uh)/as_frizz_fp(65535.0f))-as_frizz_fp(1.65f))/as_frizz_fp(0.0627f);
#if 0
	// convert accl(16G)
	data[ 1] = frizz_div((coeff * as_frizz_fp(s_buff[0].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));
	data[ 2] = frizz_div((coeff * as_frizz_fp(s_buff[1].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));
	data[ 3] = frizz_div((coeff * as_frizz_fp(s_buff[2].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));
#elif 1
	// convert accl(5G)
	data[ 1] = frizz_div((coeff * as_frizz_fp(s_buff[0].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.1914f));
	data[ 2] = frizz_div((coeff * as_frizz_fp(s_buff[1].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.1914f));
	data[ 3] = frizz_div((coeff * as_frizz_fp(s_buff[2].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.1914f));
#else
	// convert accl(200G)
	data[ 1] = frizz_div((coeff * as_frizz_fp(s_buff[0].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0065f));
	data[ 2] = frizz_div((coeff * as_frizz_fp(s_buff[1].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0065f));
	data[ 3] = frizz_div((coeff * as_frizz_fp(s_buff[2].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0065f));

#endif

#if 0
	// convet gyro(1500dps)
	//data[ 5] = ((as_frizz_fp(3.3f)*as_frizz_fp(s_buff[3].uh)/as_frizz_fp(65535.0f))-as_frizz_fp(1.5f))/as_frizz_fp(0.0008f);
	//data[ 6] = ((as_frizz_fp(3.3f)*as_frizz_fp(s_buff[4].uh)/as_frizz_fp(65535.0f))-as_frizz_fp(1.5f))/as_frizz_fp(0.0008f);
	//data[ 7] = ((as_frizz_fp(3.3f)*as_frizz_fp(s_buff[5].uh)/as_frizz_fp(65535.0f))-as_frizz_fp(1.5f))/as_frizz_fp(0.0008f);
	data[ 5] = frizz_div((coeff * as_frizz_fp(s_buff[3].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));
	data[ 6] = frizz_div((coeff * as_frizz_fp(s_buff[4].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));
	data[ 7] = frizz_div((coeff * as_frizz_fp(s_buff[5].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));
#elif 1 //300dps
	data[ 5] = frizz_div((coeff * as_frizz_fp(s_buff[3].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.003624f));
	data[ 6] = frizz_div((coeff * as_frizz_fp(s_buff[4].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.003624f));
	data[ 7] = frizz_div((coeff * as_frizz_fp(s_buff[5].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.003624f));
#else
	//6000dps
	data[ 5] = frizz_div((coeff * as_frizz_fp(s_buff[3].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.000167f));
	data[ 6] = frizz_div((coeff * as_frizz_fp(s_buff[4].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.000167f));
	data[ 7] = frizz_div((coeff * as_frizz_fp(s_buff[5].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.000167f));
#endif

	// convert magnerometer (10Gauss) ±10000LSB
	//data[ 9] = as_frizz_fp(s_buff[6].half) * as_frizz_fp(0.1f) / as_frizz_fp(100.0f);
	//data[10] = as_frizz_fp(s_buff[7].half) * as_frizz_fp(0.1f) / as_frizz_fp(100.0f);
	//data[11] = as_frizz_fp(s_buff[8].half) * as_frizz_fp(0.1f) / as_frizz_fp(100.0f);
	//data[ 9] = as_frizz_fp(s_buff[6].half) / FRIZZ_CONST_THOUSAND;
	//data[10] = as_frizz_fp(s_buff[7].half) / FRIZZ_CONST_THOUSAND;
	//data[11] = as_frizz_fp(s_buff[8].half) / FRIZZ_CONST_THOUSAND;

	//地磁気の方角がx軸とy軸がケース上とちがうために入れ替え
	data[ 9] = as_frizz_fp(s_buff[7].half) / FRIZZ_CONST_THOUSAND;
	data[10] = as_frizz_fp(s_buff[6].half) / FRIZZ_CONST_THOUSAND;
	data[11] = as_frizz_fp(s_buff[8].half) / FRIZZ_CONST_THOUSAND;
#endif

#if 1

	frizz_fp coeff;
	coeff = frizz_div(as_frizz_fp(3.3f), as_frizz_fp(65535.0f));
	// convert accl(16G)
	data[ 1] = frizz_div((coeff * as_frizz_fp(s_buff[0].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));
	data[ 2] = frizz_div((coeff * as_frizz_fp(s_buff[1].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));
	data[ 3] = frizz_div((coeff * as_frizz_fp(s_buff[2].uh)) -as_frizz_fp(1.65f), as_frizz_fp(0.0627f));

	//1500dps
	data[ 5] = frizz_div((coeff * as_frizz_fp(s_buff[3].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));
	data[ 6] = frizz_div((coeff * as_frizz_fp(s_buff[4].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));
	data[ 7] = frizz_div((coeff * as_frizz_fp(s_buff[5].uh)) -as_frizz_fp(1.5f), as_frizz_fp(0.0008f));

	//mag
	data[ 9] = as_frizz_fp(s_buff[6].half) / FRIZZ_CONST_THOUSAND;
	data[10] = as_frizz_fp(s_buff[7].half) / FRIZZ_CONST_THOUSAND;
	data[11] = as_frizz_fp(s_buff[8].half) / FRIZZ_CONST_THOUSAND;
#endif

	g_ads1191.lasttime_data[0] = data[0];
	g_ads1191.lasttime_data[1] = data[1];
	g_ads1191.lasttime_data[2] = data[2];
	g_ads1191.lasttime_data[3] = data[3];
	g_ads1191.lasttime_data[4] = data[4];
	g_ads1191.lasttime_data[5] = data[5];

	return RESULT_SUCCESS_CONV;
}

int ads1191_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
			( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_ads1191.setting.map_x		= setting->map_x;
		g_ads1191.setting.map_y		= setting->map_y;
		g_ads1191.setting.map_z		= setting->map_z;
		g_ads1191.setting.negate_x	= setting->negate_x;
		g_ads1191.setting.negate_y	= setting->negate_y;
		g_ads1191.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int ads1191_get_condition( void *data )
{
	return g_ads1191.device_condition;
}

int ads1191_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( ((unsigned short)(g_ads1191.buff[1])) << 8 ) | g_ads1191.buff[0] );
	temp_buffer[1] = ( short )( ( ((unsigned short)(g_ads1191.buff[3])) << 8 ) | g_ads1191.buff[2] );
	temp_buffer[2] = ( short )( ( ((unsigned short)(g_ads1191.buff[5])) << 8 ) | g_ads1191.buff[4] );



	return	RESULT_SUCCESS_SET;
}

unsigned int ads1191_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int ads1191_get_name()
{
	return	D_DRIVER_NAME;
}



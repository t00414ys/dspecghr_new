/*!******************************************************************************
 * @file    lis2dh.c
 * @brief   lis2dh sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "lis2dh.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_LIS2DH


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version

typedef struct {
	frizz_fp				scale;		// scaler
	unsigned char			buff[6];	// transmission buffer

	setting_direction_t		setting;
	// ** detect a device error **
	unsigned int			device_condition;
	int						recv_result;
	frizz_fp				lasttime_data[3];
	//
} lis2dh_accl_sensor_t;

struct {
	lis2dh_accl_sensor_t	accl;
	unsigned char			pwr_mgmt;
	unsigned char			addr;
	unsigned char			device_id;
} g_lis2dh;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	LIS2DH_I2C_ADDRESS_H,
	LIS2DH_I2C_ADDRESS_L,
};

int lis2dh_init( unsigned int param )
{
	int					ret, i;
	unsigned char		buff;
	unsigned char		data;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	// parameter
	// for Accelerometer
	g_lis2dh.accl.scale = as_frizz_fp( LIS2DH_ACCEL_PER_LSB );
	g_lis2dh.pwr_mgmt = 0x00;		//power down  mode

	/* setting direction default set */

	if( param == 0 ) {
		g_lis2dh.accl.setting.map_x	= DEFAULT_MAP_X;
		g_lis2dh.accl.setting.map_y	= DEFAULT_MAP_Y;
		g_lis2dh.accl.setting.map_z	= DEFAULT_MAP_Z;
		g_lis2dh.accl.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_lis2dh.accl.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_lis2dh.accl.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_lis2dh.accl.setting.map_x,	g_lis2dh.accl.setting.map_y,	g_lis2dh.accl.setting.map_z,
					g_lis2dh.accl.setting.negate_x, g_lis2dh.accl.setting.negate_y, g_lis2dh.accl.setting.negate_z );
	}

	// recognition
	// LIS2DH
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_lis2dh.addr =  add_tbl[i];
		g_lis2dh.device_id = 0;			// read id
		ret = i2c_read( g_lis2dh.addr, LIS2DH_WHO_AM_I, &g_lis2dh.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( g_lis2dh.device_id == LIS2DH_I_AM_MASK ) {
				break;
			}
		}
	}

	if( g_lis2dh.device_id != LIS2DH_I_AM_MASK ) {
		return RESULT_ERR_INIT;
	}
	// @@

	// setting part 1
	// CTRL_REG1	ODR(3-0) | LPen | Zen | Yen | Xen
	data = LIS2DH_ODR_100Hz | LIS2DH_XYZ_Enable;
	ret = i2c_write( g_lis2dh.addr, LIS2DH_CTRL_REG1, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	// setting part 2
	// CTRL_REG4	BDU | BLE | FS(1-0) | HR | ST(1-0) | SIM
	data = LIS2DH_ACCEL_ACC_RANGE | LIS2DH_HR_MASK;
	ret = i2c_write( g_lis2dh.addr, LIS2DH_CTRL_REG4, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	// setting part 3
	//power down
	buff = 0;
	i2c_read( g_lis2dh.addr, LIS2DH_CTRL_REG1, &buff, 1 );
	data = buff | LIS2DH_PowerDown;
	ret = i2c_write( g_lis2dh.addr, LIS2DH_CTRL_REG1, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void lis2dh_ctrl_accl( int f_ena )
{
	unsigned char		buff;
	unsigned char		data;

	if( f_ena ) {
		g_lis2dh.pwr_mgmt = LIS2DH_ODR_100Hz;
	} else {
		g_lis2dh.pwr_mgmt = LIS2DH_PowerDown;
	}

	buff = 0;
	i2c_read( g_lis2dh.addr, LIS2DH_CTRL_REG1, &buff, 1 );
	data = buff | g_lis2dh.pwr_mgmt;
	i2c_write( g_lis2dh.addr, LIS2DH_CTRL_REG1, &data, 1 );
}

unsigned int lis2dh_rcv_accl( unsigned int tick )
{
	g_lis2dh.accl.recv_result = i2c_read( g_lis2dh.addr, LIS2DH_OUT_X_L | 0x80, &g_lis2dh.accl.buff[0], 6 );
	/*
		g_lis2dh.accl.recv_result = i2c_read(g_lis2dh.addr, LIS2DH_OUT_X_L, &g_lis2dh.accl.buff[0], 1);
		g_lis2dh.accl.recv_result = i2c_read(g_lis2dh.addr, LIS2DH_OUT_X_H, &g_lis2dh.accl.buff[1], 1);
		g_lis2dh.accl.recv_result = i2c_read(g_lis2dh.addr, LIS2DH_OUT_Y_L, &g_lis2dh.accl.buff[2], 1);
		g_lis2dh.accl.recv_result = i2c_read(g_lis2dh.addr, LIS2DH_OUT_Y_H, &g_lis2dh.accl.buff[3], 1);
		g_lis2dh.accl.recv_result = i2c_read(g_lis2dh.addr, LIS2DH_OUT_Z_L, &g_lis2dh.accl.buff[4], 1);
		g_lis2dh.accl.recv_result = i2c_read(g_lis2dh.addr, LIS2DH_OUT_Z_H, &g_lis2dh.accl.buff[5], 1);
	*/
	if( g_lis2dh.accl.recv_result == 0 ) {
		g_lis2dh.accl.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_lis2dh.accl.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}

	return 0;
}

int lis2dh_conv_accl( frizz_fp *data )
{
	frizz_fp4w			f4w_buff;					// x:0, y:1, z:2
	frizz_fp			*fz = ( frizz_fp* )&f4w_buff;
	float				*fp = ( float* )&f4w_buff;
	sensor_util_half_t	s_buff[3];					// x:0, y:1, z:2

	if( g_lis2dh.accl.recv_result != 0 ) {
		data[0] = g_lis2dh.accl.lasttime_data[0];
		data[1] = g_lis2dh.accl.lasttime_data[1];
		data[2] = g_lis2dh.accl.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	s_buff[0].ubyte[0] = g_lis2dh.accl.buff[0];
	s_buff[0].ubyte[1] = g_lis2dh.accl.buff[1];
	s_buff[1].ubyte[0] = g_lis2dh.accl.buff[2];
	s_buff[1].ubyte[1] = g_lis2dh.accl.buff[3];
	s_buff[2].ubyte[0] = g_lis2dh.accl.buff[4];
	s_buff[2].ubyte[1] = g_lis2dh.accl.buff[5];

	// to 4way float
	// {Dx, Dy, Dz} = {Sy, -Sx, Sz}
	fp[0] = ( float )( g_lis2dh.accl.setting.negate_x ? ( -s_buff[g_lis2dh.accl.setting.map_x].half ) : ( s_buff[g_lis2dh.accl.setting.map_x].half ) );
	fp[1] = ( float )( g_lis2dh.accl.setting.negate_y ? ( -s_buff[g_lis2dh.accl.setting.map_y].half ) : ( s_buff[g_lis2dh.accl.setting.map_y].half ) );
	fp[2] = ( float )( g_lis2dh.accl.setting.negate_z ? ( -s_buff[g_lis2dh.accl.setting.map_z].half ) : ( s_buff[g_lis2dh.accl.setting.map_z].half ) );
	// convert
	f4w_buff = g_lis2dh.accl.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];

	g_lis2dh.accl.lasttime_data[0] = data[0];
	g_lis2dh.accl.lasttime_data[1] = data[1];
	g_lis2dh.accl.lasttime_data[2] = data[2];

	return RESULT_SUCCESS_CONV;
}

int lis2dh_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_lis2dh.accl.setting.map_x	= setting->map_x;
		g_lis2dh.accl.setting.map_y	= setting->map_y;
		g_lis2dh.accl.setting.map_z	= setting->map_z;
		g_lis2dh.accl.setting.negate_x	= setting->negate_x;
		g_lis2dh.accl.setting.negate_y	= setting->negate_y;
		g_lis2dh.accl.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

unsigned int lis2dh_get_ver( void )
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int lis2dh_get_name( void )
{
	return	D_DRIVER_NAME;
}

int lis2dh_accl_get_condition( void *data )
{
	return g_lis2dh.accl.device_condition;
}

int lis2dh_accl_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_lis2dh.accl.buff[1] << 8 ) | g_lis2dh.accl.buff[0] );
	temp_buffer[1] = ( short )( ( g_lis2dh.accl.buff[3] << 8 ) | g_lis2dh.accl.buff[2] );
	temp_buffer[2] = ( short )( ( g_lis2dh.accl.buff[5] << 8 ) | g_lis2dh.accl.buff[4] );

	return	RESULT_SUCCESS_SET;
}


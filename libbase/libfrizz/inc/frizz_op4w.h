/*!******************************************************************************
 * @file  frizz_op4w.h
 * @brief Operator Overload for Frizz Float Type
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_OP4W_H__
#define __FRIZZ_OP4W_H__

#if !defined(RUN_ON_PC)
#include <xtensa/tie/frizz_simd4w_que.h>

typedef union {
	frizz_tie_fp4w	vp;
	frizz_tie_fp	fr[4];
	float			fp[4];
} _frizz_op4w_union;

/* overload operator */
// negate
static inline frizz_tie_fp4w __xt_operator_NEGATE( frizz_tie_fp4w a )
{
	_frizz_op4w_union u;
	u.fp[0] = 0.0;
	u.fp[1] = 0.0;
	u.fp[2] = 0.0;
	u.fp[3] = 0.0;
	return frizz_tie_ppsub4w( u.vp, a );
}
// +
static inline frizz_tie_fp4w __xt_operator_PLUS( frizz_tie_fp4w a, frizz_tie_fp4w b )
{
	return frizz_tie_ppadd4w( a, b );
}
// -
static inline frizz_tie_fp4w __xt_operator_MINUS( frizz_tie_fp4w a, frizz_tie_fp4w b )
{
	return frizz_tie_ppsub4w( a, b );
}
// *
static inline frizz_tie_fp4w __xt_operator_MULT( frizz_tie_fp4w a, frizz_tie_fp4w b )
{
	return frizz_tie_ppmul4w( a, b );
}
// mix +
static inline frizz_tie_fp4w __xt_operator_PLUS( frizz_tie_fp a, frizz_tie_fp4w b )
{
	_frizz_op4w_union u;
	u.fr[0] = a;
	u.fr[1] = a;
	u.fr[2] = a;
	u.fr[3] = a;
	return frizz_tie_ppadd4w( u.vp, b );
}
static inline frizz_tie_fp4w __xt_operator_PLUS( frizz_tie_fp4w a, frizz_tie_fp b )
{
	return b + a;
}
// mix -
static inline frizz_tie_fp4w __xt_operator_MINUS( frizz_tie_fp a, frizz_tie_fp4w b )
{
	_frizz_op4w_union u;
	u.fr[0] = a;
	u.fr[1] = a;
	u.fr[2] = a;
	u.fr[3] = a;
	return frizz_tie_ppsub4w( u.vp, b );
}
static inline frizz_tie_fp4w __xt_operator_MINUS( frizz_tie_fp4w a, frizz_tie_fp b )
{
	return -b + a;
}
// mix *
static inline frizz_tie_fp4w __xt_operator_MULT( frizz_tie_fp a, frizz_tie_fp4w b )
{
	return frizz_tie_spmul4w( a, b );
}
static inline frizz_tie_fp4w __xt_operator_MULT( frizz_tie_fp4w a, frizz_tie_fp b )
{
	return b * a;
}

static inline frizz_tie_fp frizz_tie_vreduc( const frizz_tie_fp4w o )
{
	_frizz_op4w_union u;
	u.vp = o;
	return ( u.fr[0] + u.fr[1] + u.fr[2] + u.fr[3] );
}
#define frizz_tie_vshift_l(pop, o, in) frizz_tie_vshift_l_func(&(pop), &(o), in)
static inline void frizz_tie_vshift_l_func( frizz_tie_fp *pop, frizz_tie_fp4w *o, const frizz_tie_fp in )
{
	_frizz_op4w_union *u = ( _frizz_op4w_union* )o;
	frizz_tie_fp src = in;
	// caution: little endian
	*pop = u->fr[3];
	u->fr[3] = u->fr[2];
	u->fr[2] = u->fr[1];
	u->fr[1] = u->fr[0];
	u->fr[0] = src;
}
#define frizz_tie_vshift_r(in, o, pop) frizz_tie_vshift_r_func(in, &(o), &(pop))
static inline void frizz_tie_vshift_r_func( const frizz_tie_fp in, frizz_tie_fp4w *o, frizz_tie_fp *pop )
{
	_frizz_op4w_union *u = ( _frizz_op4w_union* )o;
	frizz_tie_fp src = in;
	// caution: little endian
	*pop = u->fr[0];
	u->fr[0] = u->fr[1];
	u->fr[1] = u->fr[2];
	u->fr[2] = u->fr[3];
	u->fr[3] = src;
}

static inline frizz_tie_fp4w frizz_tie_vneg( frizz_tie_fp4w a, int imm )
{
	frizz_tie_fp* p = ( frizz_tie_fp* )&a;
	p[0] = ( imm & 1 ) ? -p[0] : p[0];
	p[1] = ( imm & 2 ) ? -p[1] : p[1];
	p[2] = ( imm & 4 ) ? -p[2] : p[2];
	p[3] = ( imm & 8 ) ? -p[3] : p[3];
	return *( frizz_tie_fp4w* )p;
}

#endif	// !defined(RUN_ON_PC)

#endif	// __FRIZZ_OP4W_H__


/*!******************************************************************************
 * @file frizz_inv_sqrt.c
 * @brief frizz_inv_sqrt() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

/**
 * @brief constant value for frizz_inv_sqrt
 */
static const float inv_sqrt_r[] = {
	1.5f,
};

/**
 * @brief Fast inverse sqrt
 *
 * @param [in] x
 *
 * @return natural 1/sqrt(x)
 *
 * @note http://en.wikipedia.org/wiki/Fast_inverse_square_root
 */
frizz_fp frizz_inv_sqrt( frizz_fp x )
{
	const frizz_fp* inv_sqrt = ( frizz_fp* )inv_sqrt_r;
	frizz_fp hx;
	union {
		frizz_fp	z;
		int			i;
	} ux;
	// set
	hx = FRIZZ_CONST_HALF * x;
	ux.z = x;
	ux.i = 0x5f3759df - ( ux.i >> 1 );
	ux.z = ux.z * ( inv_sqrt[0] - ( hx * ux.z * ux.z ) );
	ux.z = ux.z * ( inv_sqrt[0] - ( hx * ux.z * ux.z ) );
	return ux.z;
}


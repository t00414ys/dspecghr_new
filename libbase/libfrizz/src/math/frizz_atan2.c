/*!******************************************************************************
 * @file frizz_atan2.c
 * @brief frizz_atan2() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

/**
 * @brief arc tangent with two parameters
 *
 * @param [in] y
 * @param [in] x
 *
 * @return arc tangent of y/x in radian, [-pi, +pi]
 */
frizz_fp frizz_atan2( frizz_fp y, frizz_fp x )
{
	frizz_fp rad;
	if( x == CONST_ZERO ) {
		if( y < CONST_ZERO ) {
			return -FRIZZ_MATH_PI_HALF;
		} else if( y == CONST_ZERO )	{
			return CONST_ZERO;
		} else {
			return FRIZZ_MATH_PI_HALF;
		}
	}
	if( y == CONST_ZERO ) {
		if( x < CONST_ZERO ) {
			return -FRIZZ_MATH_PI;
		} else {
			return CONST_ZERO;
		}
	}
	rad = frizz_atan( frizz_div( y, x ) ) ;
	rad = ( ( x < CONST_ZERO ) && ( y > CONST_ZERO ) ) ? ( rad + FRIZZ_MATH_PI ) :
		  ( ( ( x < CONST_ZERO ) && ( y < CONST_ZERO ) ) ? ( rad - FRIZZ_MATH_PI ) : rad );
	return rad;
}


/*!******************************************************************************
 * @file frizz_mem.c
 * @brief FRIZZ Library for dynamic memory allocation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "mcc_mem.h"
#include "frizz_mem.h"

#define USE_NEWLIB_MALLOC
#ifdef USE_NEWLIB_MALLOC
#include <malloc.h>
#endif

unsigned char	*g_frizz_malloc_area = 0;
unsigned int	g_frizz_malloc_size = 0;
unsigned long	g_total_malloc_size = 0;
unsigned char *g_frizz_malloc_max = 0;

void *frizz_malloc( unsigned int size )
{
#ifdef USE_NEWLIB_MALLOC
	void *mem;
	g_total_malloc_size += size;
	mem = memalign( FRIZZ_MEM_ALIGN_BYTE, size );
	if( g_frizz_malloc_max < ( ( unsigned char * ) mem + size + D_STACK_USE_MARGIN ) ) {
		g_frizz_malloc_max = ( unsigned char * ) mem + size + D_STACK_USE_MARGIN;
	}
	return mem;
#else
	void *mem;
	static int init = 0;
	if( init == 0 ) {
		mcc_mem_init( g_frizz_malloc_area, g_frizz_malloc_size );
		init = 1;
	}
	mem = mcc_mem_malloc( ( unsigned int )FRIZZ_MEM_ALIGN_BYTE, ( unsigned int )size );
	return mem;
#endif
}

void frizz_free( void *p )
{
#ifdef USE_NEWLIB_MALLOC
	free( p );
#else
	mcc_mem_free( p );
#endif
}


/*!******************************************************************************
 * @file  hubhal_clk_calibrate.c
 * @brief to calibrate internal OSC and check calibration value
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <string.h>
#include "timer.h"
#include <xtensa/hal.h>
#include "hubhal_clk_calibrate.h"
#include "hubhal_block.h"
#include "frizz_peri.h"
#include "frizz_env.h"
#include "frizz_util.h"
#include "gpio.h"

#if defined(USE_OSC_CALIBRATION) && !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)

static st_clk_calibrate_info_t clk_calibrate_info;
unsigned int gpio_cb_check_count = 0;
unsigned int g_pulse_width;
eGPIO_NO g_pulse_gpio_num;
eINT_NO	g_calib_int_num;
volatile int g_timeout_flag = 0;

/**
 * @brief callback of gpio interrupt,record cycle information for clk_correct
 *
 * @return void
 *
 */
void clk_calibrate_gpio_cb( void* args )
{
	// ROSC1 latch awakeM bit[2]
	*REGAWAKEM = ( *REGAWAKEM | ( 1 << 2 ) );

	if( clk_calibrate_info.gpio_cb_count % 2 == 0 ) {
		// ROSC2
		clk_calibrate_info.rise_cycle_count = xthal_get_ccount();
		while( ( *REGAWAKEM & 0x04 ) == 0x04 );	// Wait until AWakeM register bit[2] become 0
		// ROSC1 latch awakeM bit2 and read awake0/1 value
		clk_calibrate_info.rise_awake_count = ( ( *REGAWAKE0 << 16 ) | *REGAWAKE1 );
		gpio_set_mode( g_pulse_gpio_num, GPIO_MODE_IN_NINT );
	} else {
		// ROSC2
		clk_calibrate_info.fall_cycle_count = xthal_get_ccount();
		while( ( *REGAWAKEM & 0x04 ) == 0x04 );	// Wait until AWakeM register bit[2] become 0
		// ROSC1 latch awakeM bit2 and read awake0/1 value
		clk_calibrate_info.fall_awake_count = ( ( *REGAWAKE0 << 16 ) | *REGAWAKE1 );
		gpio_set_interrupt( 0 );
	}

	clk_calibrate_info.gpio_cb_count++;

	return;
}

/**
 * @brief callback of timer interrupt for calibration missing
 *
 * @param [in] count			timer counter (not use)
 * @return void
 *
 */
void clk_calibrate_timer_cb( void* args, int count )
{
	g_timeout_flag = 1;
	return;
}

/**
 * @brief set parameter for calibration
 *
 * @param [in] pulse_width		pulse width from host to frizz
 * @return success or failed
 *
 */
int set_calibration_param( unsigned int pulse_width )
{
	// parameter check
	if( ( pulse_width == 0 ) || ( D_PULSE_WIDTH_MAX < pulse_width ) ) {
		return CALIB_RET_E_PARAMETERS;
	}

	// initialize parameter
	g_pulse_width = pulse_width;
	g_pulse_gpio_num = CALIBRATION_HOST_TO_FRIZZ;

	// interrupt from frizz to HOST
	gpio_set_mode( CALIBRATION_FRIZZ_TO_HOST, GPIO_MODE_OUT );

	// set timer
	timer_init( TIMER_ID_CALIBRATION, clk_calibrate_timer_cb, 0 );

	// trigger awake timer
	hubhal_block_init( 0 );				// awake0/1 timer MAX value

	// Please set following functions after hubhal_block_init() because it would change the GPIO state.
	// set gpio for rosc calibration
	gpio_init( clk_calibrate_gpio_cb, 0 );
	gpio_set_mode( g_pulse_gpio_num, GPIO_MODE_IN_PINT );

	return CALIB_RET_S_SUCCESS;
}

/**
 * @brief	calculate core frequency according to input pulse
 *
 * @param [in,out] pre_core_freq		OSC struction for correction value
 * @return success or failed
 *
 */
int clk_calibrate( st_internal_osc_t* pre_core_freq )
{
	// set gpio isr mask
	gpio_set_interrupt( 1 );

	// start timer for gpio interrupt
	timer_start( TIMER_ID_CALIBRATION, pre_core_freq->rosc2, D_CALIB_TIMEOUT_VALUE );

	gpio_set_data( ( eGPIO_NO )CALIBRATION_FRIZZ_TO_HOST, 1 );	// gpio low -> high

	while( ( clk_calibrate_info.gpio_cb_count < 2 ) && ( g_timeout_flag == 0 ) );

	gpio_set_data( ( eGPIO_NO )CALIBRATION_FRIZZ_TO_HOST, 0 );	// gpio high -> low
	timer_stop( TIMER_ID_CALIBRATION );

	if( g_timeout_flag == 1 ) {
		g_timeout_flag = 0;
		return CALIB_RET_E_TIMEOUT;
	}

	pre_core_freq->rosc1 = ( clk_calibrate_info.rise_awake_count - clk_calibrate_info.fall_awake_count ) * ( 1000 / g_pulse_width );
	pre_core_freq->rosc2 = ( clk_calibrate_info.fall_cycle_count - clk_calibrate_info.rise_cycle_count ) * ( 1000 / g_pulse_width );

	// +/- 35% check
	if( ( pre_core_freq->rosc1 < D_DEFAULT_ROSC1_MIN ) || ( D_DEFAULT_ROSC1_MAX < pre_core_freq->rosc1 )
		|| ( pre_core_freq->rosc2 < D_DEFAULT_ROSC2_MIN ) || ( D_DEFAULT_ROSC2_MAX < pre_core_freq->rosc2 ) ) {
		return CALIB_RET_E_OVER_FREQ;
	}

	// reset for next use
	memset( &clk_calibrate_info, 0, sizeof( clk_calibrate_info ) );

	// clear gpio interrupt
	gpio_set_interrupt( 0 );
	gpio_init( 0, 0 );

	return CALIB_RET_S_SUCCESS;
}

#endif	// defined(USE_OSC_CALIBRATION) && !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)


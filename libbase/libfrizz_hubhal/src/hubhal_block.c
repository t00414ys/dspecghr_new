/*!******************************************************************************
 * @file hubhal_block.c
 * @brief source code for hubhal
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/xtruntime.h>
#include <xtensa/hal.h>
#include <xtensa/config/core.h>
#include <xtensa/config/system.h>
#include <xtensa/tie/xt_interrupt.h>
#include <stdio.h>
#include <string.h>
#if defined(RUN_ON_PC)
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <sys/time.h>
#endif

#include "frizz_peri.h"
#include "frizz_env.h"
#include "frizz_util.h"
#include "gpio.h"
#include "hubhal_in_state.h"
#include "hubhal_block.h"
#include "hub_mgr_if.h"
#include "frizz_env.h"

use_mode_t use_mode;
#if defined(USE_HOST_API_UART_IN)
#include "hubhal_host_api.h"
#endif
extern unsigned int	g_ROSC2_FREQ;
extern unsigned int	g_ROSC1_FREQ;

#if	defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)
EXTERN_C void OpenCommandFile( void );
#endif

hubhal_block_info_t g_hubhal_block;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@
// @@					Default boot mode
// @@
const struct st_frizz_mode	frizz_ini_mode0 = {
	// @@@ unsigned int    start_delimiter;     		// search for mark;
	0x40404040,											// '@@@@'
	// @@@ unsigned int    customer_id;     		    // customer number
	FRIZZ_CUSTOMER_ID,									// default customer_id
	// @@@ unsigned int    frizz_version;				// frizz_version
	( FRIZZ_VER_MAJOR << 24 ) | ( FRIZZ_VER_MINOR << 16 ) | ( FRIZZ_VER_DETAIL <<  0 ),
	// @@@ unsigned int    clock_source;				// external(0) or internal(!0)
#if  !defined(USE_INTERNAL_CLOCK)
	0,													// EXTERNAL CLOCK
#else
	1,													// INTERNAL CLOCK
#endif
	// @@@ unsigned int    internal_mode;				// high sleep(0,other) , low sleep(1) , low stop(2)
#if	defined(USE_MODE_SLEEP_LOW_CLOCK)
	LOW_SPEED_SLEEP_MODE,								// LOW SLEEP
#elif defined(USE_MODE_STOP_LOW_CLOCK)
	LOW_SPEED_STOP_MODE,								// LOP STOP
#else
	HIGH_SPEED_SLEEP_MODE,								// HIGH SLEEP
#endif
	// @@@ unsigned int    awake_cycle;					// 1~1000(D_AWAKE_INT_CYCLE_MAX)
	D_AWAKE_INT_CYCLE,
#if defined(USE_INTERNAL_CLOCK)
	// @@@ unsigned int    high_sleep_switch_source;	// GPIO to determine the switching of the high-speed processing(0~3)
	LOW_SPEED_GPIO,
	// @@@ unsigned int    awaker_osc1_enable_gpio;		// change low speed mode gpio
	AWAKER_OSC1_ENABLE_GPIO,
#else
	0,
	0,
#endif
	// @@@ unsigned int    bomb_for_test;				// use Time bomb to testing firmware
#if defined(USE_TIME_BOMB_FOR_TEST_FW)
	0,													// not message
#else
	D_NO_BOMB_MARK,										// safe
#endif
	// @@@ unsigned int    test_fw_timer_ms;			// bomb timer
#if defined(TEST_FW_TIMER_MS)
	TEST_FW_TIMER_MS,
#else
	( 12 * 60 * 60 * 1000 ),
#endif
	// @@@ char            guard_string[16];			// guard string
	{0},
	// @@@ unsigned int    start_delimiter;     		// search for mark;
	0x40404040											// '@@@@'
};
__attribute__( ( aligned( 16 ) ) ) const char	frizz_ini_mode1[] = { "####" __DATE__ __TIME__ };

volatile	struct st_frizz_mode	frizz_use_mode;

// This can not be debugger connection it's low clock (The grace period from the start-up 30 seconds)
#define		D_LOW_CLOCK_PERMIT_TIME		(30*1000)
EXTERN_C void hubhal_awake_init( unsigned int freq, unsigned int cycle_ms );

#if !defined(RUN_ON_PC)
//--- unhandled_exception routine ----------------------------------------------------
#include <xtensa/tie/frizz_simd4w_que.h>
#include "hubhal_out.h"

#define HUBHAL_FORMAT_HUNG_CODE			0xFF7FFF00
#define	UNHANDLED_EXCEPTION_SAVE_SIZE	(7)		// SAVE data is exccause,excvaddr,epc1,epc2,epc3,epc4,pc5
//
typedef void ( *funcptr )( void );
extern funcptr	_unhandled_exception_func;
extern unsigned int	*_unhandled_exception_save;
unsigned int	unhandled_exception_save_data[UNHANDLED_EXCEPTION_SAVE_SIZE];

// It is called from an exception handler.
// Since it operates in user mode, please describe the shortest processing.
static void unhandled_exception_isr( void )
{
	unsigned int *p = ( unsigned int* ) _unhandled_exception_save;
	int						i;
#if !defined(RUN_ON_XPLORER)
	gpio_set_data( ( eGPIO_NO )g_hubhal_out.no, g_hubhal_out.default_level );
	oq_sensor_out_push( ( HUBHAL_FORMAT_HUNG_CODE | UNHANDLED_EXCEPTION_SAVE_SIZE ) );
	for( i = 0 ; i < 7 ; i++ ) {
		oq_sensor_out_push( p[i] );
	}
	gpio_set_data( ( eGPIO_NO )g_hubhal_out.no, g_hubhal_out.default_level ^ 0x1 );
#else
	printf( "frizz HUNG\n" );
	for( i = 0 ; i < 7 ; i++ ) {
		printf( "x%08X\n", p[i] );
	}
#endif

	return;   //  It will return to the exception handler
}

//***//
void unhandled_exception_init( void )
{
	_unhandled_exception_func = unhandled_exception_isr;
	unhandled_exception_save_data[0] = 0xFFFFFFFF;
	_unhandled_exception_save = &unhandled_exception_save_data[0];
}
//------------------------------------------------------------
#endif	// !defined(RUN_ON_PC)



static void awake_isr( void )
{
	// to change high clock mode has already changed in the int-highpri-dispatcher.S file with an assembler (library libs/libxtos_int4.a)
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) & (~0x0800);	// internal high clock OSC power on
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) | 0x0200;	// to high clock mode
#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
	*REGXMODE1 &= ( ~0x0040 );						// Set Sleep Mode

	// to clear interrupt
	*REGAWAKEM |= 0x8;
#endif

	check_frizz_times( 0, 0 );

	// update timestamp
	g_hubhal_block.ts += g_hubhal_block.sys_tick;

	if( frizz_use_mode.bomb_for_test != D_NO_BOMB_MARK ) {
		if( g_hubhal_block.ts > frizz_use_mode.test_fw_timer_ms ) {
			while( 1 );
		}
	}

#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
	while( ( *REGAWAKEM & 0x20 ) == 0x20 );	// Wait until AWakeM register bit[5] interrupt monitor become 0
#endif
}


#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
// This Interrupt Use LOW_SPEED_SLEEP_MODE
static volatile int	gpio_int_cond_flag = 0;
static void lowsleep_awake_up_event( void *ptr )
{
	// to change high clock mode has already changed in the int-lowpri-dispatcher.S file with an assembler (library libs/libxtos_int1.a)
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) & (~0x0800);	// internal high clock OSC power on
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) | 0x0200;	// to high clock mod
	gpio_int_cond_flag = 1;
	gpio_set_mode( ( eGPIO_NO ) use_mode.gpio_no, GPIO_MODE_IN );
}

/**
 * @brief awake timer initialize
 *
 * @param [in] freq using clock [Hz]
 */
void hubhal_awake_init( unsigned int freq, unsigned int cycle_ms )
{
	// stop module
	*REGAWAKEM &= 0xFFFE;
	_xtos_ints_off( 1 << INTERRUPT_NO_AWAKE );

	// register callback
	_xtos_set_interrupt_handler( INTERRUPT_NO_AWAKE, ( _xtos_handler )awake_isr );

	// set to start
	if( freq == 0 ) {
		*REGAWAKE0 = 0xFFFF;		//max value for osc calibration
		*REGAWAKE1 = 0xFFFF;
		g_hubhal_block.sys_tick = 0;
	} else {
		freq /= 1000;				// to msec order
		freq *= cycle_ms;			// to awake cycle
		if( freq < 65535 ) {
			*REGAWAKE0 = 1 - 1;		// every 1 msec
			*REGAWAKE1 = freq - 1;
			g_hubhal_block.sys_tick = cycle_ms;
		} else {
			*REGAWAKE0 = ( freq >> 15 ) - 1;		// every ?? msec
			*REGAWAKE1 = ( freq & 0xFFFF ) - 1;

			g_hubhal_block.sys_tick = cycle_ms;
		}
	}

	*REGAWAKEM =
		( 1		<< 4 ) |		// counter load
		( 1		<< 3 ) |		// clear interrupt
		( 0		<< 1 ) |		// always 0 for Cyclic
		( 0		<< 0 ) ;		// disable module

	*REGAWAKEM =
		( 1		<< 4 ) |		// counter load
		( 1		<< 3 ) |		// clear interrupt
		( 0		<< 1 ) |		// always 0 for Cyclic
		( 1		<< 0 ) ;		// enable module

	_xtos_ints_on( 1 << INTERRUPT_NO_AWAKE );
}
#elif defined(RUN_ON_PC_EXEC)	// !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
static void sigcatch( int sig )
{
	fprintf( stderr, "catch signal %dn", sig );
	exit( 1 );
}
#endif	// defined(RUN_ON_PC_EXEC)

/**
 * @brief awake timer initialize
 *
 * @param [in] freq using clock [Hz]
 */
void hubhal_block_init( unsigned int freq )
{
#if !defined(RUN_ON_PC)
	unhandled_exception_init();
#endif
	use_mode.power_mode_flag = HIGH_SPEED_SLEEP_MODE;

#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
#if 1
	if( frizz_use_mode.clock_source != 0 ) {		// Internal Clock
		int		i;
		*REGXMODE1 |= 0x0080;	// Set AWake timer clock source to ROSC01(100KHz)
		if( frizz_use_mode.internal_mode == LOW_SPEED_SLEEP_MODE ) {
			hubhal_set_power_mode( LOW_SPEED_SLEEP_MODE, frizz_use_mode.high_sleep_switch_source );
		} else if( frizz_use_mode.internal_mode == LOW_SPEED_STOP_MODE ) {
			hubhal_set_power_mode( LOW_SPEED_STOP_MODE, frizz_use_mode.high_sleep_switch_source );
		}
		//
		gpio_set_mode( ( eGPIO_NO )frizz_use_mode.awaker_osc1_enable_gpio, GPIO_MODE_OUT );
		for( i = 0 ; i < ( 100 * 2 ) ; i++ ) {
			gpio_toggle_data( ( eGPIO_NO ) frizz_use_mode.awaker_osc1_enable_gpio );
		}
	} else {
		*REGXMODE1 &= ~0x0080;	// Set AWake timer clock source to ROSC02(40MHz)
	}
#else
	frizz_use_mode.clock_source = 0;
	*REGXMODE1 &= ~0x0080;	// Set AWake timer clock source to ROSC02(40MHz)
#endif
	if( ( *REGXMODE0 & 0x0001 ) != 0 ) {
		// self boot ( frizz clock mode = stand alone )
		*REGALT_FUNC |= 0x0400;
	} else {
		// host boot ( frizz clock mode = host mode )
		*REGALT_FUNC &= ~0x0400;
	}
#endif	// !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)

#if defined(USE_TIME_BOMB_FOR_TEST_FW)
	// If "USE_TIME_BOMB_FOR_TEST_FW" is defined, it is checked guard string
	if( strncmp( frizz_ini_mode0.guard_string, frizz_ini_mode1, sizeof( frizz_ini_mode0.guard_string ) ) != 0 ) {
		// Enable bomb if the guard character is wrong
		frizz_use_mode.bomb_for_test = 0;
	}
#endif

	// clear timestamp
	g_hubhal_block.ts = 0;
#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
	hubhal_awake_init( freq, frizz_use_mode.awake_cycle );
#else
#if	defined(RUN_ON_PC_EXEC)
	signal( SIGINT, sigcatch );
#endif
	OpenCommandFile();
	g_hubhal_block.sys_tick = frizz_use_mode.awake_cycle;
#endif
}

/**
 * @brief get timestamp
 *
 * @return current timestamp
 */
unsigned int hubhal_block_get_ts( void )
{
	return g_hubhal_block.ts;
}



/**
 * @brief blocking for target timestamp or interrupt
 *
 * @return current timestamp
 */
unsigned int hubhal_block_exec( unsigned int target_ts, unsigned int target_output_num )
#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
{
	int				diff;
	int				mode_change = 0;
	int				bit = 0;
	static int		low_clock_permit = 0;
#if defined(USE_SUPER_POWER_SAVING)
	static int 		awake_change = 1;
	int				diff_1;
#endif
	_xtos_set_intlevel( INTERRUPT_LEVEL_AWAKE );	// enable interrupt level 4
	diff = target_ts - g_hubhal_block.ts;
	diff = 1; //20170913 temporal
#if defined(USE_SUPER_POWER_SAVING)
	diff_1 = diff;
#endif

	if( target_output_num == 0 ) {
		// no wait for num of output
		target_output_num = 0x0FFFFFFF;
	}
	target_output_num += 2;	// add header and timestamp
	while( 0 < diff && g_hubhal_in.state != HUBHAL_IN_STATE_CMD && ( ( 64 - *REGFIFO_CNT ) < target_output_num ) ) {
		bit = 0;
		mode_change = 0;

		if( low_clock_permit == 0 ) {
			if( g_hubhal_block.ts > D_LOW_CLOCK_PERMIT_TIME ) {
				low_clock_permit = 1;
			}
		}

		if( use_mode.power_mode_flag == HIGH_SPEED_SLEEP_MODE ) {
			/////////////////////
			// High Speed Sleep Mode
			_xtos_set_intlevel( INTERRUPT_LEVEL_EXP );
			*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) & ( ~0x0800 );	// internal high clock OSC power on
			*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) | 0x0200;	// to high clock mode
			*REGXMODE1 &= ( ~0x0040 );						// Set Sleep Mode
			_xtos_set_intlevel( INTERRUPT_LEVEL_AWAKE );
		} else if( use_mode.power_mode_flag == LOW_SPEED_SLEEP_MODE ) {

			gpio_int_cond_flag = 0;
			gpio_set_mode( ( eGPIO_NO )use_mode.gpio_no, GPIO_MODE_IN );			// gpio input
			bit = gpio_get_data( ( eGPIO_NO )use_mode.gpio_no );

#if defined(USE_SUPER_POWER_SAVING)
			if( diff_1 >= ( D_AWAKE_INT_CYCLE_MAX - 100 ) ) {	// 100 is running time.
				if( ( awake_change != 0 ) && ( bit == 0 ) ) {
					awake_change = 0;
					hubhal_awake_init( g_ROSC1_FREQ, D_AWAKE_INT_CYCLE_MAX );
				}
			} else {
				if( awake_change != 1 ) {
					awake_change = 1;
					hubhal_awake_init( g_ROSC1_FREQ, frizz_use_mode.awake_cycle );
				}
			}
#endif
			// when it enables host side gpio, high speed mode will be maintained as it is.
			// Movement normal until the first awake outbreak
#if	defined(USE_LOW_SPEED_FIFO_CHK)
			if( ( ( low_clock_permit != 0 ) && ( ( *REGALT_FUNC & 0x0400 ) != 0 ) ) ||
				( ( ( *REGALT_FUNC & 0x0400 ) == 0 ) && ( bit != 0x01 ) && ( ( *REGXMODE0 & 0x0400 ) == 0 ) && ( *REGFIFO_CNT == 0 ) ) )
#else
			if( ( ( low_clock_permit != 0 ) && ( ( *REGALT_FUNC & 0x0400 ) != 0 ) ) ||
				( ( ( *REGALT_FUNC & 0x0400 ) == 0 ) && ( bit != 0x01 ) && ( ( *REGXMODE0 & 0x0400 ) == 0 ) ) )
#endif
			{
				/////////////////////
				// Low Speed Sleep Mode
				_xtos_set_intlevel( INTERRUPT_LEVEL_EXP );
				gpio_set_mode( ( eGPIO_NO )use_mode.gpio_no, GPIO_MODE_IN_PINT );	// Interrupt Setting
				gpio_init( lowsleep_awake_up_event, 0 );
				gpio_set_interrupt( ENABLING_INTERRUPT );

				// set low sleep mode
				*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) & ~0x0200;	// to low clock mode
				*REGXMODE1 &= ( ~0x0040 );						// Set Sleep Mode
				*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) | 0x0800;	// internal high clock OSC power down
				_xtos_set_intlevel( INTERRUPT_LEVEL_GPIO3 );			// enable interrupt level 1
				mode_change = 1;
			} else {
				/////////////////////
				// High Speed Sleep Mode
				_xtos_set_intlevel( INTERRUPT_LEVEL_EXP );
				*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) & ( ~0x0800 );	// internal high clock OSC power on
				*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) | 0x0200;	// to high clock mode
				*REGXMODE1 &= ( ~0x0040 );						// Set Sleep Mode
				_xtos_set_intlevel( INTERRUPT_LEVEL_AWAKE );
				mode_change = 0;
			}
		} else if( use_mode.power_mode_flag == LOW_SPEED_STOP_MODE ) {
			gpio_set_mode( ( eGPIO_NO )use_mode.gpio_no, GPIO_MODE_IN );			// gpio input
			bit = gpio_get_data( ( eGPIO_NO )use_mode.gpio_no );
			// when it enables host side gpio, high speel mode will be maintained as it is.
			_xtos_set_intlevel( INTERRUPT_LEVEL_EXP );
#if	defined(USE_LOW_SPEED_FIFO_CHK)
			if( ( ( low_clock_permit != 0 ) && ( ( *REGALT_FUNC & 0x0400 ) != 0 ) ) ||
				( ( ( *REGALT_FUNC & 0x0400 ) == 0 ) && ( bit != 0x01 ) && ( ( *REGXMODE0 & 0x0400 ) == 0 ) && ( *REGFIFO_CNT == 0 ) ) )
#else
			if( ( ( low_clock_permit != 0 ) && ( ( *REGALT_FUNC & 0x0400 ) != 0 ) ) ||
				( ( ( *REGALT_FUNC & 0x0400 ) == 0 ) && ( bit != 0x01 ) && ( ( *REGXMODE0 & 0x0400 ) == 0 ) ) )
#endif
			{
				/////////////////////
				// High Speed Stop Mode
				*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) & ~0x0200;		// to low clock mode
				*REGXMODE1 |= 0x0040;								// Set Stop Mode
			} else {
				/////////////////////
				// High Speed Sleep Mode
				*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) & ( ~0x0800 );	// internal high clock OSC power on
				*REGXMODE0 = ( *REGXMODE0 & 0x0A00 ) | 0x0200;	// to high clock mode
				*REGXMODE1 &= ( ~0x0040 );						// Set Sleep Mode
			}
			_xtos_set_intlevel( INTERRUPT_LEVEL_AWAKE );
		} else {
			//non
		}

		if( use_mode.power_mode_flag == LOW_SPEED_SLEEP_MODE ) {
			// WAIT does not do "low sleep mode and GPIO interrupt" by here
			if( ( mode_change == 0 ) || ( gpio_int_cond_flag == 0 ) ) {
				// if gpio is low ---> wait ,  gpio is high ---> not wait(low sleep mode only)
				XT_WAITI( 0 );
			}
		} else {
			XT_WAITI( 0 );
		}

		_xtos_set_intlevel( INTERRUPT_LEVEL_AWAKE );
		diff = target_ts - g_hubhal_block.ts;
		if( gpio_int_cond_flag == 1 ) {
			break;
		}

#if defined(USE_HOST_API_UART_IN)
		hubhal_host_uart_in();
#endif
	}
	_xtos_set_intlevel( 0 );

	return g_hubhal_block.ts;
}
#else	// !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
{
	int				diff;
#if defined(RUN_ON_PC_EXEC)
	struct timespec  req;
#endif

	diff = target_ts - g_hubhal_block.ts;
	if( diff >= 0 ) {
		awake_isr();
	} else {
		while( 0 < diff && g_hubhal_in.state != HUBHAL_IN_STATE_CMD ) {
			///////
#if defined(RUN_ON_PC_EXEC)
			req.tv_sec  = 0;
			req.tv_nsec = g_hubhal_block.sys_tick * D_FRIZZ_TICK_NS;
			nanosleep( &req, NULL );
#endif
			awake_isr();
			///////
			diff = target_ts - g_hubhal_block.ts;
		}
	}

	return g_hubhal_block.ts;
}
#endif	// !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)



/**
 * @brief setting power mode
 *
 * @return int status
 */
int hubhal_set_power_mode( unsigned int mode, unsigned int gpio_number )
{
	if( ( mode == HIGH_SPEED_SLEEP_MODE ) || ( mode == LOW_SPEED_SLEEP_MODE ) || ( mode == LOW_SPEED_STOP_MODE ) ) {
		use_mode.power_mode_flag = mode;
		use_mode.gpio_no = gpio_number;

		if( ( use_mode.power_mode_flag == LOW_SPEED_SLEEP_MODE ) || ( use_mode.power_mode_flag == LOW_SPEED_STOP_MODE ) ) {
			gpio_set_mode( ( eGPIO_NO )use_mode.gpio_no, GPIO_MODE_IN );
#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
			if( ( *REGXMODE0 & 0x0400 ) != 0 ) {
				DEBUG_PRINT( "[warning] Power Mode = %d , XMODE0 = x%04X\r\n", use_mode.power_mode_flag, *REGXMODE0 );
			}
#endif
		}
		return 0;	//success
	}
	return -1;	//false
}


void check_frizz_times( unsigned long *sec_cnt, unsigned long *nsec_cnt )
{
	static unsigned long	previous_ccount = 0;
	static unsigned long	sec_count = 0;
	static unsigned long	clock_to_nano = 0;
	unsigned long			clk = 0;
#if defined(RUN_ON_PC)
	struct timeval			tv;
#endif
	if( clock_to_nano == 0 ) {
		if( frizz_use_mode.clock_source == 0 ) {				// External Clock
			clock_to_nano = 1000000000UL / g_ROSC2_FREQ;		// Clock to nano sec
		} else {
			clock_to_nano = 1000;								// tick to nano sec
		}
	}

	if( frizz_use_mode.clock_source == 0 ) {		// External Clock
#if !defined(RUN_ON_PC)
		__asm__( "rsr.ccount %0" : "=a"( clk ) );
#else
		gettimeofday( &tv, NULL );
		clk = tv.tv_usec % 40000000;
#endif
		clk = clk % g_ROSC2_FREQ;
	} else {
		clk = g_hubhal_block.ts % 1000;
	}

	if( clk < previous_ccount ) {
		sec_count++;
	}
	previous_ccount = clk;
	if( sec_cnt != 0 ) {
		*sec_cnt  = sec_count;
	}
	if( nsec_cnt != 0 ) {
		*nsec_cnt = previous_ccount * clock_to_nano;
	}
	return;
}


/*!******************************************************************************
 * @file    pac7673ee_api.h
 * @brief   pac7673ee sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PAC7673EE_API_H__
#define __PAC7673EE_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int pac7673ee_init( unsigned int param );

void pac7673ee_ctrl_prox( int f_ena );
void pac7673ee_ctrl_light( int f_ena );

unsigned int pac7673ee_rcv_prox( unsigned int tick );
int pac7673ee_conv_prox( frizz_fp *prox_data );

unsigned int pac7673ee_rcv_light( unsigned int tick );
int pac7673ee_conv_light( frizz_fp *light_data );

int pac7673ee_setparam( void *ptr );
unsigned int pac7673ee_get_ver( void );
unsigned int pac7673ee_get_name( void );

int pac7673ee_light_get_condition( void *data );
int pac7673ee_prox_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif // __PAC7673EE_H__

